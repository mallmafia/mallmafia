var express = require("express"),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require("mongoose"),
  User = require("./api/models/userModel"),
  bodyParser = require("body-parser"),
  expressValidator = require("express-validator"),
  jsonwebtoken = require("jsonwebtoken"),
  morgan = require("morgan"),
  multer = require("multer"),
  { Pool, Client } = require("pg"),
  connectionString =
    "postgresql://postgres:martbee@database.server.com:3211/martdb";

const pool = new Pool({
  connectionString: connectionString
});

mongoose.connect(
  process.env.MONGODB_URI || "mongodb://localhost/heroku_z2v5h0sp"
);
mongoose.Promise = global.Promise;
app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(expressValidator());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public/");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
const upload = multer({ storage: storage });

app.use(function(req, res, next) {
  if (
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "JWT"
  ) {
    jsonwebtoken.verify(
      req.headers.authorization.split(" ")[1],
      "we-are-the-martbee-developers",
      function(err, decode) {
        if (err) req.user = undefined;
        req.user = decode;
        next();
      }
    );
  } else {
    req.user = undefined;
    next();
  }
});
var routes = require("./api/routes/mallRoutes");
routes(app);

app.use(function(req, res) {
  res.status(404).send({ url: req.originalUrl + " not found" });
});

app.listen(port);

console.log("todo list RESTful API server started on: " + port);

module.exports = app;
