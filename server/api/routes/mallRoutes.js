module.exports = function(app) {
  var mallController = require("../controllers/malls"),
    seedController = require("../controllers/seedController"),
    userController = require("../controllers/user"),
    cartController = require("../controllers/cart"),
    storeController = require("../controllers/store"),
    productController = require("../controllers/product"),
    multer = require("multer"),
    sellerController = require("../controllers/sellerController");
  const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, "./public/sites/products");
    },
    filename: function(req, file, cb) {
      cb(null, file.originalname);
    }
  });
  const upload = multer({ storage: storage });

  //configuring routes
  app.route("/api/mall").get(mallController.getMalls);
  app.route("/api/mall").get(mallController.getMall);
  app.route("/api/auth/register").post(userController.register);
  app.route("/api/auth/sign-in").post(userController.sign_in);
  app.route("/api/store").get(storeController.getStores);
  // app.route("/api/store/:storeId").get(storeController.getMallStores);
  app.route("/api/seed").get(seedController.crawl);
  app.route("/api/mall/products").get(productController.getMallProducts);
  app.route("/api/store/products").get(productController.getStoreProducts);
};
