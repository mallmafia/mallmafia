var Crawler = require("crawler"),
  db = require("../db");
var safeParse = require("safe-json-parse/tuple");
let sampledata = [];
exports.crawl = (req, respo) => {
  var c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function(error, res, done) {
      if (error) {
        console.log(error);
      } else {
        var $ = res.$;

        var data = {
          name: $("#productTitle")
            .text()
            .trim(),
          quantity: Math.floor(Math.random() * (50 - 10 + 1)) + 10,
          price: parseInt(
            $("#priceblock_ourprice")
              .text()
              .replace(/,/g, "")
              .slice(0, -3)
              .trim()
          )
            ? parseInt(
                $("#priceblock_ourprice")
                  .text()
                  .replace(/,/g, "")
                  .slice(0, -3)
                  .trim()
              )
            : parseInt(
                $("#priceblock_dealprice")
                  .text()
                  .replace(/,/g, "")
                  .slice(0, -3)
                  .trim()
              )
              ? parseInt(
                  $("#priceblock_dealprice")
                    .text()
                    .replace(/,/g, "")
                    .slice(0, -3)
                    .trim()
                )
              : parseInt(
                  $("#priceblock_saleprice")
                    .text()
                    .replace(/,/g, "")
                    .slice(0, -3)
                    .trim()
                ),
          showcase: "d",
          store_id: 212,
          mall_id: 2,
          is_product_active: true,
          category_id: 1,
          description: $(
            ".a-unordered-list.a-vertical.a-spacing-none, #feature-bullets"
          )
            .text()
            .trim()
            .replace(/(\r\n|\n|\r)/gm, ""),
          // imgs: Object.keys(
          //   safeParse($("#landingImage").attr("data-a-dynamic-image"))[1]
          // ),
          store_name: "The Watch Store",
          rating: Math.floor(Math.random() * (5 - 1 + 1)) + 1,
        };
        // console.log();
        console.log($("#landingImage").attr("data-a-dynamic-image"));

        db.query(
          `INSERT INTO product
            (name, description, quantity, store_id, mall_id, is_product_active, price, stars, product_imgs, product_main_img)
             VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
          [
            data.name,
            data.description.replace(/\s\s+/g, " "),
            data.quantity,
            5,
            2,
            true,
            data.price,
            data.rating,
            JSON.stringify(
              Object.keys(
                safeParse($("#landingImage").attr("data-a-dynamic-image"))[1]
              )
            ),
            Object.keys(
              safeParse($("#landingImage").attr("data-a-dynamic-image"))[1]
            )[0]
          ],
          (err, dbdata) => {
            if (err) {
              console.log("err");
              // console.log(err);
              return;
            } else {
              console.log("Successfully");
              return;
            }
          }
        );
      }

      done();
    },
  });

  // Queue just one URL, with default callback
  c.queue([
    "https://www.amazon.in/Woodland-Mens-Khaki-Leather-Sneakers/dp/B01FVW5OA2/ref=sr_1_2?ie=UTF8&qid=1507108759&sr=8-2&keywords=Woodland",
    "https://www.amazon.in/Woodland-Olive-Green-Leather-Boots/dp/B00II0JMA8/ref=sr_1_3?ie=UTF8&qid=1507108759&sr=8-3&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Khaki-Leather-Boots/dp/B00V8NFB8O/ref=sr_1_4?ie=UTF8&qid=1507108759&sr=8-4&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Khaki-Leather-Sneakers/dp/B00YN2PSWQ/ref=sr_1_6?ie=UTF8&qid=1507108759&sr=8-6&keywords=Woodland",
    "https://www.amazon.in/Woodland-Cut-Style-Leather-Tan-Wallet/dp/B073Y69CX8/ref=sr_1_5?ie=UTF8&qid=1507108759&sr=8-5&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Camel-Leather-Sneakers/dp/B01LSTNATY/ref=sr_1_8?ie=UTF8&qid=1507108759&sr=8-8&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Camel-Leather-Sneakers/dp/B00II0KM84/ref=sr_1_9?ie=UTF8&qid=1507108759&sr=8-9&keywords=Woodland",
    "https://www.amazon.in/Woodland-Camel-Leather-Loafers-Mocassins/dp/B01F5HEK9Y/ref=sr_1_10?ie=UTF8&qid=1507108759&sr=8-10&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-leather-belt-size/dp/B06XSGR659/ref=sr_1_11?ie=UTF8&qid=1507108759&sr=8-11&keywords=Woodland",
    "https://www.amazon.in/Woodland-Camel-Leather-Sandals-Floaters/dp/B018M2D35Q/ref=sr_1_12?ie=UTF8&qid=1507108759&sr=8-12&keywords=Woodland",
    "https://www.amazon.in/Woodland-Brown-Mens-Wallet-513008/dp/B019WFSDDY/ref=sr_1_13?ie=UTF8&qid=1507108759&sr=8-13&keywords=Woodland",
    "https://www.amazon.in/Woodland-Olive-Green-Leather-Boots/dp/B00II0LEZ4/ref=sr_1_14?ie=UTF8&qid=1507108759&sr=8-14&keywords=Woodland",
    "https://www.amazon.in/Woodland-Olive-Green-Leather-Sneakers/dp/B01LHDVZWK/ref=sr_1_15?ie=UTF8&qid=1507108759&sr=8-15&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Khaki-Leather-Sneakers/dp/B01M3VPHD0/ref=sr_1_16?ie=UTF8&qid=1507108759&sr=8-16&keywords=Woodland",
    "https://www.amazon.in/Woodland-Black-Nubuck-Leather-Sneakers/dp/B00II0L4QS/ref=sr_1_17?ie=UTF8&qid=1507108800&sr=8-17&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Grey-Trecking-Shoe/dp/B01D9P57PU/ref=sr_1_18?ie=UTF8&qid=1507108800&sr=8-18&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Camel-Leather-Sneakers/dp/B01LSTNPYO/ref=sr_1_19?ie=UTF8&qid=1507108800&sr=8-19&keywords=Woodland",
    "https://www.amazon.in/Woodland-Genuine-Leather-Mens-Wallet/dp/B072BHXYXZ/ref=sr_1_20?ie=UTF8&qid=1507108800&sr=8-20&keywords=Woodland",
    "https://www.amazon.in/Woodland-Khaki-Leather-Espadrille-Flats/dp/B01HS3PC0O/ref=sr_1_21?ie=UTF8&qid=1507108800&sr=8-21&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Snaype-Leather-Boots/dp/B01D4ILXNM/ref=sr_1_22?ie=UTF8&qid=1507108800&sr=8-22&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Tobacco-Leather-Sneakers/dp/B019E5UTIY/ref=sr_1_23?ie=UTF8&qid=1507108800&sr=8-23&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Khaki-Leather-Sneakers/dp/B018M2CN5C/ref=sr_1_24?ie=UTF8&qid=1507108800&sr=8-24&keywords=Woodland",
    "https://www.amazon.in/Woodland-Mens-Camel-Leather-Sneakers/dp/B015J5CGHA/ref=sr_1_25?ie=UTF8&qid=1507108800&sr=8-25&keywords=Woodland"
  ]);
};
