var db = require("../db"),
  jwt = require("jsonwebtoken"),
  bcrypt = require("bcrypt"),
  async = require("async"),
  crypto = require("crypto");

//Controller to register a user
exports.register = (req, res) => {
  //Server side validation for email, password and mobile number
  req
    .assert("email", "Please check your email id")
    .notEmpty()
    .isEmail();
  req
    .assert("phone", "Please check your mobile Number")
    .isNumeric()
    .len(10, 10);
  req
    .assert("password", "Password must be 5-20 characters")
    .notEmpty()
    .len(5, 20);

  var errors = req.validationErrors();
  if (errors) {
    console.log(errors);
    return res.json({
      status: 404,
      results: errors[0].msg
    });
  }
  //Inserting the email, phone , hashedpassword, full_name into the user db
  db.query(
    "INSERT INTO users (email, phone, password, full_name) VALUES ($1, $2, $3, $4)",
    [
      req.body.email,
      req.body.phone,
      bcrypt.hashSync(req.body.password, 10),
      req.body.full_name
    ],
    (err, data) => {
      if (err) {
        console.log(err);
        return res.json(err);
      }
      res.send({ status: 200, message: "Successfully Registered" });
    }
  );
};

//controller to sign in
exports.sign_in = (req, res) => {
  const { email, password } = req.body;
  //query the user with email and comparing the password and sending JWT
  db.query("SELECT * FROM users WHERE email = $1", [email], (err, results) => {
    const data = results.rows[0];
    if (err) return next(err);
    else if (!data)
      return res.json({
        status: 404,
        message: `${email} not found. Kindly register`
      });
    else if (data) {
      // comparing the passwords with hashed password
      bcrypt.compare(password, data.password, (err, bres) => {
        //bres is bycrypt response
        if (bres) {
          return res.json({
            status: 200,
            token: jwt.sign(
              {
                email: data.email,
                full_name: data.full_name,
                id: data.id,
                phone: data.phone
              },
              "we-are-the-martbee-developers"
            )
          });
        } else {
          return res.json({
            status: 404,
            message: `Please sign-in again`
          });
        }
      });
    }
  });
};
