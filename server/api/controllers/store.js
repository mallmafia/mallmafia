var db = require("../db");

exports.getStores = (req, res) => {
  db.query("SELECT * FROM store", (err, stores) => {
    if (err) {
      res.json({ status: 404, error: err });
    }
    res.json({ status: 200, stores: stores.rows });
  });
};

// exports.getStore = (req,res)=>{
//   db.query("SELECT")
// }

exports.getMallStores = (req, res) => {
  db.query(
    `SELECT  store.*
    FROM store
    WHERE store.id =$1`,
    [req.params.storeId],
    (err, data) => {
      if (err) {
        console.log(err);
        return res.json(err);
      }
      res.send(data.rows);
    }
  );
};
