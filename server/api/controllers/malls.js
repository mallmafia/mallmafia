var db = require("../db");

exports.getMalls = (req, res) => {
  //Query to get all rows from mall table
  db.query("SELECT * FROM mall", (err, data) => {
    if (err) {
      return res.json(err);
    }
    res.send(data.rows);
  });
};

exports.getMall = (req, res) => {
  //Query to get a joined data of a mall & mall_banner_img table
  db.query(
    `SELECT  mall.*
    FROM mall
    WHERE mall.id=$1
    `,
    [req.params.mallId],
    (err, data) => {
      if (err) {
        console.log(err);
        return res.send(err);
      }
      res.send(data.rows);
    }
  );
};
