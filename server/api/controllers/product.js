var db = require("../db");

exports.getMallProducts = (req, res) => {
  db.query(
    "SELECT * FROM product WHERE mall_id=$1",
    [req.query.mallId],
    (err, data) => {
      if (err) {
        console.log(err);
        return res.send(err);
      }
      res.send(data.rows);
    }
  );
};

exports.getStoreProducts = (req, res) => {
  db.query(
    "SELECT * FROM product WHERE store_id=$1",
    [req.query.storeId],
    (err, data) => {
      if (err) {
        console.log(err);
        return res.send(err);
      }
      res.send(data.rows);
    }
  );
};
