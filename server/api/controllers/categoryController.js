var mongoose = require("mongoose"),
  Mall = require("../models/mallModel"),
  Category = require("../models/categoryModel");

exports.getCategory = function(req, res) {
  var mallId = req.params.mallId,
    page = req.params.pageNo;
  Mall.findOne({ id: mallId }, function(err, mall) {
    var num = 3;
    var limit = page * num;
    var skip = page > 1 ? --page * num : 0;
    Category.aggregate(
      {
        $match: {
          id: { $in: mall.category_ids }
        }
      },
      { $limit: limit },
      { $skip: skip },
      function(err, category) {
        if (err) {
          res.send(err);
        }
        res.json(category);
      }
    );
  });
};

exports.addCategory = function(req, res) {
  var newCategory = new Category(req.body);
  console.log(req.body);
  newCategory.save(function(err, category) {
    if (err) {
      res.send(err);
    }
    res.json(category);
  });
};
