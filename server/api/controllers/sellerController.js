var mongoose = require("mongoose"),
  path = require("path"),
  Product = require("../models/productModel"),
  Orders = require("../models/orderModel"),
  jwt = require("jsonwebtoken"),
  bcrypt = require("bcrypt"),
  path = require("path"),
  async = require("async"),
  crypto = require("crypto"),
  _ = require("lodash"),
  hbs = require("nodemailer-express-handlebars"),
  email = "tomatomafias@gmail.com",
  pass = "tomato12345",
  nodemailer = require("nodemailer"),
  Seller = require("../models/sellerModel");

exports.sellerHome = function(req, res) {
  res.sendFile(path.resolve("./../client/web/index.html"));
};

// exports.addProduct = function(req, res) {
//   console.log("product page");
//   res.sendFile(path.resolve("./../client/web/addproduct.html"));
// };

exports.addProduct = function(req, res) {
  if (req.user) {
    var newProduct = new Product(req.body);
    newProduct.save(function(err, product) {
      if (err) {
        res.send(err);
      }
      Store.findOneAndUpdate(
        { _id: req.user.store_id },
        { $addToSet: { product_ids: product._id } },
        { upsert: true, new: true },
        (err, store) => {
          if (err) {
            res.json({
              status: 402,
              error: err
            });
          }
        }
      );
      res.json(product);
    });
  } else {
    res.json({
      status: 404,
      results: "seller not found"
    });
  }
};
exports.addToStore = function(req, res) {
  var newProduct = new Product(req.body);
  console.log(req.file);
  newProduct.save(function(err, product) {
    if (err) {
      res.json({
        status: 402,
        error: err
      });
    }
    res.json(product);
  });
};
exports.sellerOrders = function(req, res) {
  if (req.user.store_id) {
    Orders.find(
      { "order_items.store_id": req.user.store_id },
      null,
      { sort: { date: -1 } },
      function(err, order) {
        if (err) {
          res.json({
            status: 402,
            error: err
          });
        }
        res.json(order);
      }
    );
  }
};
exports.updateOrder = (req, res) => {
  if (req.user) {
    Orders.findOneAndUpdate(
      { _id: req.query.orderId },
      { order_status: req.body.orderStatus },
      { upsert: true, new: true },
      (err, order) => {
        if (err) {
          res.json({
            status: 402,
            error: err
          });
        }
        res.json(order);
      }
    );
  } else {
    return res.json(null);
  }
};
exports.getActiveProducts = function(req, res) {
  if (req.user) {
    Product.find(
      { store_id: req.user.store_id, is_product_active: true },
      (err, products) => {
        if (err) {
          res.json({
            status: 402,
            error: err
          });
        }
        res.json(products);
      }
    );
  } else {
    return res.json(null);
  }
};
exports.getSeller = (req, res) => {
  if (req.user) {
    Seller.findOne({ _id: req.user._id }, function(err, user) {
      return res.json({
        status: 200,
        user: user
      });
    });
  } else {
    return res.json(null);
  }
};
exports.updateProduct = (req, res) => {
  if (req.user) {
    Product.findOneAndUpdate(
      { _id: req.query.productId, store_id: req.user.store_id },
      { $set: req.body },
      { upsert: true, new: true },
      (err, product) => {
        if (err) {
          res.json({
            status: 402,
            error: err
          });
        }
        res.json(product);
      }
    );
  } else {
    return res.json(null);
  }
};

exports.registerSeller = function(req, res) {
  req
    .assert("email", "Please check your email id")
    .notEmpty()
    .isEmail();
  req
    .assert("phone", "Please check your mobile Number")
    .isNumeric()
    .len(10, 10);
  req
    .assert("password", "Password must be 5-20 characters")
    .notEmpty()
    .len(5, 20);

  var errors = req.validationErrors();
  if (errors) {
    return res.json({
      status: 422,
      results: errors[0].msg
    });
  }
  var newSeller = new Seller(req.body);
  newSeller.hash_password = bcrypt.hashSync(req.body.password, 10);
  newSeller.save(function(err, seller) {
    if (err) {
      if (err.code === 11000) {
        return res.json({
          status: 11000,
          results: "This Email already registered"
        });
      } else {
        return res.json({
          status: 404,
          results: "Network issues",
          error: err
        });
      }
    } else {
      seller.hash_password = undefined;
      return res.json({ status: 200, results: seller });
    }
  });
};
exports.sign_in = function(req, res) {
  Seller.findOne(
    {
      email: req.body.email
    },
    function(err, seller) {
      if (err) {
        res.json({
          status: 402,
          error: err
        });
      }
      if (!seller || !seller.comparePassword(req.body.password)) {
        return res.json({
          status: 401,
          message: "Authentication failed. Invalid seller or password."
        });
      } else {
        return res.json({
          status: 200,
          token: jwt.sign(
            {
              email: seller.email,
              fullName: seller.fullName,
              _id: seller._id,
              store_id: seller.store_id
            },
            "RESTFULAPIs"
          )
        });
      }
    }
  );
};

exports.updateOrderStatus = (req, res) => {
  if (req.user) {
    Orders.findOneAndUpdate(
      { _id: req.params.orderId, "order_items.productId": req.body.productId },
      {
        $set: {
          "order_items.$.order_status": req.body.order_status
        }
      },
      { upsert: true, new: true },
      function(err, orders) {
        if (err) {
          res.send(err);
        } else {
          res.send(orders.order_items);
        }
      }
    );
  }
};

exports.getOrderItems = (req, res) => {
  if (req.user) {
    Orders.findOne({ _id: req.params.orderId }, function(err, order) {
      if (err) {
        res.send(err);
      } else {
        res.send(order.order_items);
      }
    });
  }
};
