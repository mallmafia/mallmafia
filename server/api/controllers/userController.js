"use strict";

var mongoose = require("mongoose"),
  jwt = require("jsonwebtoken"),
  bcrypt = require("bcrypt"),
  User = mongoose.model("User"),
  Product = require("../models/productModel"),
  Order = require("../models/orderModel"),
  path = require("path"),
  async = require("async"),
  crypto = require("crypto"),
  _ = require("lodash"),
  hbs = require("nodemailer-express-handlebars"),
  email = "tomatomafias@gmail.com",
  pass = "tomato12345",
  nodemailer = require("nodemailer");

var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: email,
    pass: pass
  }
});

var handlebarsOptions = {
  viewEngine: "handlebars",
  viewPath: path.resolve("./api/templates/"),
  extName: ".html"
};

smtpTransport.use("compile", hbs(handlebarsOptions));

exports.register = function(req, res) {
  req
    .assert("email", "Please check your email id")
    .notEmpty()
    .isEmail();
  req
    .assert("phone", "Please check your mobile Number")
    .isNumeric()
    .len(10, 10);
  req
    .assert("password", "Password must be 5-20 characters")
    .notEmpty()
    .len(5, 20);

  var errors = req.validationErrors();
  if (errors) {
    console.log(errors);
    return res.json({
      status: 422,
      results: errors[0].msg
    });
  }

  var newUser = new User(req.body);
  newUser.hash_password = bcrypt.hashSync(req.body.password, 10);
  newUser.cart = [];
  newUser.save(function(err, user) {
    if (err) {
      console.log(err);
      if (err.code === 11000) {
        return res.json({
          err,
          status: 11000,
          results: "This Email already registered"
        });
      }
      return res.json({
        status: 404,
        results: "Network issues"
      });
    } else {
      user.hash_password = undefined;
      var data = {
        to: user.email,
        from: email,
        template: "welcome",
        subject: "Welcome to martBee",
        context: {
          // url: "http://mallmafia.herokuapp.com/api/auth/welcomeuser",
          name: user.fullName.split(" ")[0]
        }
      };

      smtpTransport.sendMail(data, function(err) {
        if (err) {
          return res.json({
            err,
            message: "Kindly check your email for further assistance"
          });
        } else {
          return res.json({ status: 200, results: user });
        }
      });
    }
  });
};
exports.welcomeuser = (req, res) => {
  return res.sendFile(path.resolve("./public/welcome.html"));
};
exports.index = function(req, res) {
  return res.sendFile(path.resolve("./public/home.html"));
};

exports.render_forgot_password_template = function(req, res) {
  return res.sendFile(path.resolve("./public/forgot-password.html"));
};

exports.render_reset_password_template = function(req, res) {
  return res.sendFile(path.resolve("./public/reset-password.html"));
};

exports.getUser = function(req, res) {
  if (req.user) {
    User.findOne({ _id: req.user._id }, function(err, user) {
      return res.json({
        cart: user.cart,
        fullName: user.fullName,
        email: user.email,
        phone: user.phone,
        cart_total_price: user.cart_total_price
      });
    });
  } else {
    return res.json();
  }
};

exports.getCart = function(req, res) {
  if (req.user) {
    User.findOne({ _id: req.user._id }, function(err, user) {
      return res.json(user.cart.map(product => product.product));
    });
  } else {
    return res.json(null);
  }
};

exports.addToCart = function(req, res) {
  var productId = req.body.productId;
  console.log(productId);
  req.assert("productId", "The productId should be a number").isNumeric();
  // var errors = req.validationErrors();
  if (req.validationErrors()) {
    return res.json({
      status: 422,
      results: errors[0].msg
    });
  }
  if (req.user) {
    Product.findOne({ _id: productId }, (err, product) => {
      User.findOne({ _id: req.user._id }, function(err, user) {
        user.cart.addToSet({
          productId: productId,
          quantity: req.body.quantity,
          store_id: product.store_id,
          product: product,
          store_name: product.store_name
        });
        user.save(function(err, data) {
          if (err) {
            return res.status(422).send({
              message: err
            });
          } else {
            user.set({
              cart_total_price: user.cart
                .map(product => product.product)
                .map(product => product.price)
                .reduce((a, b) => a + b, 0)
            });
            user.save();
            return res.json(user.cart.map(product => product.product));
          }
        });
      });
    });
  }
};

function cartProducts(res, user, cartData) {
  Product.find(
    { _id: { $in: user.cart.map(items => items.productId) } },
    function(err, products) {
      user.set({
        cart_total_price: products
          .map(
            product =>
              product.price *
              cartData.cart.filter(cart => cart.productId == product.id)[0]
                .quantity
          )
          .reduce((a, b) => a + b, 0)
      });
      user.save();
      return res.json(products);
    }
  );
}

exports.updateCart = (req, res) => {
  var removeId = req.body.productId,
    quantity = req.body.quantity;
  if (req.user) {
    if (quantity) {
      Product.findOne({ _id: removeId }, function(err, product) {
        if (quantity < product.quantity) {
          User.findOneAndUpdate(
            { _id: req.user._id, "cart.productId": removeId },
            { $set: { "cart.$.quantity": quantity } },
            { upsert: true, new: true },
            function(err, use) {
              if (err) {
                res.json(err);
              } else {
                cartProducts(res, use, use);
              }
            }
          );
        } else {
          User.findOne({ _id: req.user._id }, function(err, userdata) {
            res.json(userdata);
          });
        }
      });
    } else {
      User.findOneAndUpdate(
        { _id: req.user._id },
        { $pull: { cart: { productId: removeId } } },
        { upsert: true, new: true },
        function(err, userr) {
          userr.set({
            cart_total_price: userr.cart
              .map(product => product.product)
              .map(product => product.price)
              .reduce((a, b) => a + b, 0)
          });
          userr.save();
          return res.json(userr.cart.map(product => product.product));
        }
      );
    }
  }
};

exports.getOrders = (req, res) => {
  if (req.user._id) {
    Order.find(
      { userId: req.user._id },
      null,
      { sort: { date: -1 } },
      (err, orders) => {
        res.json(orders);
      }
    );
  }
};

exports.placeOrder = function(req, res) {
  if (req.user) {
    User.findOne({ _id: req.user._id }, (err, user) => {
      if (user.cart.length > 0) {
        var order = new Order({
          userId: req.user._id,
          user_phone: req.user.phone,
          order_items: user.cart,
          order_total: user.cart_total_price,
          order_type: req.body.order_type
        });
        order.save((err, newOrder) => {
          if (err) {
            res.json(err);
          } else {
            user.set({ cart: [], cart_total_price: 0 });
            user.save((err, orderUser) => {
              if (err) {
                console.log(err);
              }
              res.json([]);
            });
          }
        });
      } else {
        console.log("Error");
        res.json({
          status: 404,
          results: "cart is empty"
        });
      }
    });
  }
};
exports.deliverProduct = (req, res) => {
  if (req.user) {
    User.findOne({ _id: req.user._id }, (err, user) => {
      if (err) {
        res.json(err);
      } else {
        res.json({
          status: 200,
          address: user.address,
          user: user
        });
      }
    });
  } else {
    res.json({
      status: 404,
      results: "User not found"
    });
  }
};
exports.addAddress = (req, res) => {
  if (req.user) {
    User.findOneAndUpdate(
      { _id: req.user._id },
      {
        $addToSet: { address: req.body },
        $set: { selected_address: req.body }
      },
      { upsert: true, new: true },
      function(err, user) {
        if (err) {
          res.json(err);
        } else {
          res.json({
            status: 200,
            address: user.address,
            selected_address: user.selected_address,
            user: user
          });
        }
      }
    );
  } else {
    res.json({
      status: 404,
      results: "User not found"
    });
  }
};

exports.editAddress = function(req, res) {
  if (req.user) {
    User.findOneAndUpdate(
      { _id: req.user._id, "address._id": req.query.addressid },
      { $set: { "address.$": req.body } },
      { upsert: true, new: true },
      (err, user) => {
        if (err) {
          res.json(err);
        } else {
          res.json({
            status: 200,
            address: user.address,
            user: user
          });
        }
      }
    );
  } else {
    res.json({
      status: 404,
      results: "User not found"
    });
  }
};
exports.sign_in = function(req, res) {
  User.findOne(
    {
      email: req.body.email
    },
    function(err, user) {
      if (err) throw err;
      if (!user || !user.comparePassword(req.body.password)) {
        return res.json({
          status: 401,
          message: "Authentication failed. Invalid user or password."
        });
      }
      return res.json({
        status: 200,
        token: jwt.sign(
          {
            email: user.email,
            fullName: user.fullName,
            _id: user._id,
            phone: user.phone
          },
          "RESTFULAPIs"
        )
      });
    }
  );
};

exports.loginRequired = function(req, res, next) {
  if (req.user) {
    next();
  } else {
    return res.json({ status: 401, message: "Unauthorized user!" });
  }
};

exports.isAdmin = function(req, res, next) {
  if (req.user.role === "admin") next();
  else return res.json({ status: 401, message: "Permission denied" });
};

exports.forgot_password = function(req, res) {
  async.waterfall(
    [
      function(done) {
        User.findOne({
          email: req.body.email
        }).exec(function(err, user) {
          if (user) {
            done(err, user);
          } else {
            done("User not found.");
          }
        });
      },
      function(user, done) {
        // create the random token
        crypto.randomBytes(20, function(err, buffer) {
          var token = buffer.toString("hex");
          done(err, user, token);
        });
      },
      function(user, token, done) {
        User.findByIdAndUpdate(
          { _id: user._id },
          {
            reset_password_token: token,
            reset_password_expires: Date.now() + 86400000
          },
          { upsert: true, new: true }
        ).exec(function(err, new_user) {
          done(err, token, new_user);
        });
      },
      function(token, user, done) {
        var data = {
          to: user.email,
          from: email,
          template: "forgot-password-email",
          subject: "Password help has arrived!",
          context: {
            url:
              "http://mallmafia.herokuapp.com/api/auth/reset_password?token=" +
              token,
            name: user.fullName.split(" ")[0]
          }
        };

        smtpTransport.sendMail(data, function(err) {
          if (!err) {
            return res.json({
              message: "Kindly check your email for further instructions"
            });
          } else {
            return done(err);
          }
        });
      }
    ],
    function(err) {
      return res.json({ status: 422, message: err });
    }
  );
};

exports.reset_password = function(req, res, next) {
  User.findOne({
    reset_password_token: req.body.token,
    reset_password_expires: {
      $gt: Date.now()
    }
  }).exec(function(err, user) {
    if (!err && user) {
      if (req.body.newPassword === req.body.verifyPassword) {
        user.hash_password = bcrypt.hashSync(req.body.newPassword, 10);
        user.reset_password_token = undefined;
        user.reset_password_expires = undefined;
        user.save(function(err) {
          if (err) {
            return res.status(422).send({
              message: err
            });
          } else {
            var data = {
              to: user.email,
              from: email,
              template: "reset-password-email",
              subject: "Password Reset Confirmation",
              context: {
                name: user.fullName.split(" ")[0]
              }
            };

            smtpTransport.sendMail(data, function(err) {
              if (!err) {
                return res.json({ message: "Password reset" });
              } else {
                return done(err);
              }
            });
          }
        });
      } else {
        return res.status(422).send({
          message: "Passwords do not match"
        });
      }
    } else {
      return res.status(400).send({
        message: "Password reset token is invalid or has expired."
      });
    }
  });
};
