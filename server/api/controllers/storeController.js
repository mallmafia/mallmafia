var mongoose = require("mongoose"),
  Mall = require("../models/mallModel"),
  Store = require("../models/storeModel");

exports.getAllStores = function(req, res) {
  var mallId = req.params.mallId;
  Store.find({ mall_id: mallId }, function(err, stores) {
    if (err) {
      res.json({
        status: 401,
        result: "Server down please wait for a while"
      });
    }
    res.json(stores);
  });
};

exports.getStore = function(req, res) {
  var storeId = req.params.storeId;
  console.log(storeId);
  Store.find({ _id: storeId }, function(err, store) {
    if (err) {
      res.json({
        status: 401,
        result: "Server down please wait for a while"
      });
    }
    res.json(store);
  });
};

exports.getCategoryStores = function(req, res) {
  var categoryId = req.params.categoryId;
  var page = req.params.pageNo;
  var num = 2;
  var limit = page * num;
  var skip = page > 1 ? (page - 1) * num : 0;
  console.log(categoryId);
  Store.aggregate(
    {
      $match: {
        category_id: categoryId
      }
    },
    { $limit: limit },
    { $skip: skip },
    function(err, stores) {
      if (err) {
        res.json({
          status: 401,
          result: "Server down please wait for a while"
        });
      }
      console.log(stores);
      res.json(stores);
    }
  );
};

exports.addStore = function(req, res) {
  var newStore = new Store(req.body);
  console.log(req.body);
  newStore.save(function(err, store) {
    if (err) {
      res.json({
        status: 401,
        result: "Server down please wait for a while"
      });
    }
    res.json(store);
  });
};
