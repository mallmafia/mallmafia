var db = require("../db");

exports.addToCart = (req, res) => {
  if (req.user) {
    db.query(
      "INSERT into cart (user_id, product_id, quantity) VALUES ($1, $2, $3)",
      [req.user.id, req.body.product_id, req.body.quantity],
      (err, data) => {
        if (err) {
          res.send(err);
        } else {
          res.send("submitted");
        }
      }
    );
  } else {
    res.send("Please sign in");
  }
};
