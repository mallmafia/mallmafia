var mongoose = require("mongoose"),
  async = require("async"),
  Mall = require("../models/mallModel"),
  Stores = require("../models/storeModel"),
  Product = require("../models/productModel"),
  Category = require("../models/categoryModel");

exports.getMall = function(req, res) {
  var mallId = req.params.mallId;
  var page = req.query.page ? parseInt(req.query.page) : 1;
  var num = 5;
  var limit = page * num;
  var skip = page > 1 ? (page - 1) * num : 0;
  Mall.findOne({ _id: mallId }, function(err, mall) {
    if (mall) {
      var mallDetail = {};
      mallDetail.mall = mall;
      //////////Searching for stores using store_ids
      async.waterfall(
        [
          function(callback) {
            Stores.aggregate(
              {
                $match: {
                  _id: { $in: mall.store_ids }
                }
              },
              { $limit: limit },
              { $skip: skip },
              function(err, stores) {
                if (err) {
                  mallDetail.stores = "Error while fetching";
                }
                mallDetail.stores = stores;
                callback(null, stores);
              }
            );
          },
          function(stores, callback) {
            Product.aggregate(
              {
                $match: {
                  _id: { $in: mall.product_ids }
                }
              },
              { $limit: limit },
              { $skip: skip },
              function(err, products) {
                if (err) {
                  mallDetail.products = "Error while fetching";
                }
                mallDetail.products = products;
                callback(null, stores, products);
              }
            );
          },
          function(stores, products, callback) {
            Product.aggregate(
              {
                $match: {
                  _id: { $in: mall.top_products }
                }
              },
              function(err, top_products) {
                if (err) {
                  mallDetail.top_products = "Error while fetching";
                }
                mallDetail.top_products = top_products;
                callback(null, stores, products, top_products);
              }
            );
          },
          function(stores, products, top_products, callback) {
            Category.aggregate(
              {
                $match: {
                  _id: { $in: mall.category_ids }
                }
              },
              function(err, category) {
                if (err) {
                  mallDetail.category = "Error while fetching";
                }
                mallDetail.category = category;
                callback(null, stores, products, top_products, category);
              }
            );
          },
          function(stores, products, top_products, category, callback) {
            Stores.aggregate(
              {
                $match: {
                  _id: { $in: mall.top_stores }
                }
              },
              function(err, top_stores) {
                if (err) {
                  mallDetail.top_stores = "Error while fetching";
                }
                mallDetail.top_stores = top_stores;
                callback(null, {
                  mall: mallDetail.mall,
                  stores: stores,
                  products: products,
                  top_products: top_products,
                  category: category,
                  top_stores: top_stores
                });
              }
            );
          }
        ],
        (err, results) => {
          if (err) {
            res.send(err);
          }
          res.send(results);
        }
      );
    } else {
      return res.json("No mall in the collection");
    }
  });
};

exports.addMall = function(req, res) {
  var newMall = new Mall(req.body);
  console.log(req.body);
  newMall.save(function(err, mall) {
    if (err) {
      res.send(err);
    }
    res.json(mall);
  });
};

exports.getAllMall = function(req, res) {
  Mall.find({}, function(err, malls) {
    if (err) {
      res.send(err);
    }
    res.json(malls);
  });
};
