var mongoose = require("mongoose"),
  Market = require("../models/mallModel"),
  Product = require("../models/productModel");

exports.getAllProducts = function(req, res) {
  var mallId = req.params.mallId;
  Product.find({ mall_id: mallId, is_product_active: true }, function(
    err,
    products
  ) {
    if (err) {
      return res.send(err);
    }
    return res.json(products);
  });
};
exports.getProductsFromIds = function(req, res) {
  var productIds = req.query.ids.split(",").map(id => Number(id));
  console.log(productIds);
  Product.find({ _id: { $in: productIds }, is_product_active: true }, function(
    err,
    products
  ) {
    if (err) {
      res.send(err);
    }
    return res.json(products);
  });
};
exports.getProduct = function(req, res) {
  var productId = req.params.productId;
  Product.find({ _id: productId, is_product_active: true }, function(
    err,
    product
  ) {
    if (err) {
      res.send(err);
    }
    res.json(product);
  });
};

exports.getCategoryProducts = function(req, res) {
  var categoryId = parseInt(req.params.categoryId);
  var page = req.params.pageNo;
  var num = 2;
  var limit = page * num;
  var skip = page > 1 ? (page - 1) * num : 0;
  console.log(categoryId);
  Product.aggregate(
    {
      $match: {
        category_id: categoryId,
        is_product_active: true
      }
    },
    { $limit: limit },
    { $skip: skip },
    function(err, products) {
      if (err) {
        res.send(err);
      }
      console.log(products);
      res.json(products);
    }
  );
};
exports.searchProduct = (req, res) => {
  Product.find(
    { $text: { $search: req.query.q, $language: "en" } },
    (err, products) => {
      if (err) {
        res.json(err);
      }
      res.json(products);
    }
  );
};
exports.addProduct = function(req, res) {
  var newProduct = new Product(req.body);
  console.log(req.body);
  newProduct.save(function(err, product) {
    if (err) {
      res.send(err);
    }
    res.json(product);
  });
};
