var mongoose = require("mongoose"),
  shortid = require("shortid"),
  Schema = mongoose.Schema;

var orderSchema = new Schema({
  _id: {
    type: String,
    default: shortid.generate
  },
  userId: { type: String, required: true },
  user_phone: { type: Number, required: true },
  order_items: [
    {
      productId: Number,
      quantity: Number,
      store_id: Number,
      product: Object,
      order_status: { type: String, default: "ordered" },
      order_message: String,
      store_name: String
    }
  ],
  order_total: Number,
  order_type: Number,
  address: String,
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Order", orderSchema);
