var mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  autoIncrement = require("mongoose-auto-increment");

var connection = mongoose.createConnection(
  process.env.MONGODB_URI || "mongodb://localhost/heroku_z2v5h0sp"
);

autoIncrement.initialize(connection);

var productSchema = new Schema({
  // id: {
  //   type: Number,
  //   required: true,
  //   unique: true,
  // },
  name: {
    type: String,
    required: true,
  },
  imgs: Array,
  rating: {
    type: Number,
    required: true,
  },
  reviews: Array,
  description: String,
  price: {
    type: Number,
    required: true,
  },
  mall_id: {
    type: Number,
    required: true,
  },
  store_id: {
    type: Number,
    required: true,
  },
  store_name: {
    type: String,
  },
  category_id: {
    type: Number,
    required: true,
  },
  showcase: {
    type: String,
    required: true,
  },
  tags: Array,
  quantity: {
    type: Number,
    required: true,
  },
  is_product_active: {
    type: Boolean,
    required: true,
  },
});

productSchema.plugin(autoIncrement.plugin, {
  model: "Products",
  field: "_id",
  startAt: 100,
});

module.exports = mongoose.model("Products", productSchema);
