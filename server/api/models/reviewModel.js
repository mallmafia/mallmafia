var mongoose = require("mongoose"),
  Schema = mongoose.Schema;

var reviewSchema = new Schema({
  reviewer_name: {type:String, required: true},
  reviewer_email: {type:String, required: true},
  review: {type:String, required: true}
});

module.exports = mongoose.model("Reviews", reviewSchema);
