var mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  autoIncrement = require("mongoose-auto-increment");

var connection = mongoose.createConnection(
  process.env.MONGODB_URI || "mongodb://localhost/heroku_z2v5h0sp"
);

autoIncrement.initialize(connection);

var categorySchema = new Schema({
  // id: Number,
  icon: String,
  name: String,
  description: String,
});

categorySchema.plugin(autoIncrement.plugin, {
  model: "Category",
  field: "_id",
  startAt: 1,
});

module.exports = mongoose.model("Category", categorySchema);
