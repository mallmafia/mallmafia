var mongoose = require("mongoose"),
bcrypt = require("bcrypt"),
  Schema = mongoose.Schema;

var sellerSchema = new Schema ({
  id: {
    type: Number,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  imgs: Array,
  description: String,
  userName: {
    type: String,
    required: true
  },
  mall_id: {
    type: Number,
    required: true
  },
  store_id: {
    type: Number,
    required: true
  },
  store_name: {
    type: String
  },
  mall_name:{
    type:String,
    required:true
  },
  phone: {
    type: Number,
    required: true,
    min: 1000000000,
    max: 9999999999
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: true
  },
  hash_password: {
    type: String,
    required: true
  }
})

sellerSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.hash_password);
};


module.exports = mongoose.model('Seller',sellerSchema);
