"use strict";

var mongoose = require("mongoose"),
  bcrypt = require("bcrypt"),
  Schema = mongoose.Schema,
  uniqueArray = require("mongoose-unique-array");

var UserSchema = new Schema({
  fullName: {
    type: String,
    trim: true,
    required: true
  },
  phone: {
    type: Number,
    required: true,
    min: 1000000000,
    max: 9999999999
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: true
  },
  hash_password: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    default: "user"
  },
  address: [
    {
      fullName: String,
      door_no: String,
      street: String,
      landmark: String,
      city: String,
      state: String,
      country: { type: String, default: "india" },
      pincode: String
    }
  ],
  cart: [
    {
      productId: { unique: true, type: Number },
      quantity: { type: Number, default: 1 },
      store_id: Number,
      product: Object,
      store_name: String
    }
  ],
  cart_total_price: Number,
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  }
});

UserSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.hash_password);
};

UserSchema.plugin(uniqueArray);
mongoose.model("User", UserSchema);
