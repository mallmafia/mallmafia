var mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  autoIncrement = require("mongoose-auto-increment");

var connection = mongoose.createConnection(
  process.env.MONGODB_URI || "mongodb://localhost/heroku_z2v5h0sp"
);

autoIncrement.initialize(connection);

var storeSchema = new Schema({
  // id: { type: Number, unique: true, required: true },
  name: {
    type: String,
    required: true,
  },
  imgs: Array,
  description: { type: String, required: true },
  mall_id: { type: Number, required: true },
  category_id: { type: Number, required: true },
  rating: { type: Number },
  tags: Array,
  reviews: Array,
  product_ids: Array,
  order_ids: Array,
  showcase: String,
});

storeSchema.plugin(autoIncrement.plugin, {
  model: "Store",
  field: "_id",
  startAt: 200,
});
module.exports = mongoose.model("Store", storeSchema);
