var mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  autoIncrement = require("mongoose-auto-increment");

var connection = mongoose.createConnection(
  process.env.MONGODB_URI || "mongodb://localhost/heroku_z2v5h0sp"
);

autoIncrement.initialize(connection);
var mallSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  location: { type: String, required: true },
  banner_imgs: Array,
  store_ids: Array,
  top_products: Array,
  category_ids: Array,
  top_stores: Array,
  product_ids: Array,
});

mallSchema.plugin(autoIncrement.plugin, {
  model: "Mall",
  field: "_id",
  startAt: 1,
});

module.exports = mongoose.model("Mall", mallSchema);
