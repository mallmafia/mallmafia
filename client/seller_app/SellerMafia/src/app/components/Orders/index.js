import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import Card from "@commons/Card";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Request } from "@data/actions";
import { Actions } from "react-native-router-flux";
import IconSmall from "./IconSmall";
import moment from "moment";
import { images } from "./images";
class Orders extends Component {
  componentDidMount() {
    if (!this.props.orders) {
      this.props.Request(
        { method: "get", route: "orders", name: "orders" },
        () => null
      );
    }
  }
  render() {
    if (!this.props.orders) {
      return (
        <View>
          <ActivityIndicator size={40} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.props.orders.map(order => (
            <View style={styles.container}>
              <Card>
                <TouchableOpacity
                  onPress={() => Actions.orderinfo({ order_details: order })}
                >
                  <View style={styles.row.first}>
                    <Text style={styles.orderid}>
                      {"#: " + order._id.toString()}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <IconSmall
                        style={styles.icon.cart}
                        img={{ uri: images.icons.phone }}
                      />
                      <Text style={styles.orderid}>
                        {order.user_phone.toString()}
                      </Text>
                    </View>

                    <Text style={styles.price}>
                      {"₹ " + order.order_total.toString()}
                    </Text>
                  </View>
                  <View style={styles.row.first}>
                    <View style={{ flexDirection: "row" }}>
                      <IconSmall
                        style={styles.icon.cart}
                        img={{ uri: images.icons.cart }}
                      />
                      <Text style={styles.orderid}>
                        {" " + order.order_items.length.toString()}
                      </Text>
                    </View>
                    <Text style={styles.orderid}>
                      {moment(order.date).format("MMM Do YY, hh:mm a")}
                    </Text>
                  </View>
                </TouchableOpacity>
              </Card>
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.Entities;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Request: Request }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(Orders);

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#eee"
  },
  row: {
    first: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      padding: 2
    },
    second: {
      flexDirection: "row",
      alignItems: "center",
      padding: 2
    }
  },
  icon: {
    cart: { tintColor: "#f44f42" }
  },
  orderid: {
    color: "#000"
  },
  price: {
    color: "#000",
    fontWeight: "500"
  }
};
