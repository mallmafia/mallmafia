import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Image,
  ActivityIndicator
} from "react-native";
import { Actions } from "react-native-router-flux";
import Card from "@commons/Card";
import moment from "moment";
import { connect } from "react-redux";
import { Request } from "@data/actions";
const { width, height } = Dimensions.get("window");

class OrderInfo extends React.Component {
  componentDidMount() {
    this.props.fetchOrderDetails(this.props.order_details._id);
  }
  render() {
    if (!this.props.orderitem) {
      return (
        <View>
          <ActivityIndicator size={40} />
        </View>
      );
    }
    let order = this.props.order_details;
    return (
      <View>
        <ScrollView>
          <Card>
            <Text>Phone: {this.props.order_details.user_phone}</Text>
            <Text>
              Ordered on : {moment(order.date).format(
                "MMM Do YY, hh:mm a"
              )}{" "}
            </Text>
            {this.props.orderitem ? (
              this.props.orderitem.map(item => (
                <View style={orderstyle.card}>
                  <View style={[orderstyle.content, { flexDirection: "row" }]}>
                    <Image
                      style={orderstyle.image}
                      source={{ uri: item.product.showcase }}
                    />
                    <View style={{ marginLeft: 5 }}>
                      <Text style={orderstyle.name}>
                        {item.product.name.slice(0, 13)}
                      </Text>
                      <Text style={orderstyle.id}>Order id: {order._id}</Text>
                      <Text style={orderstyle.id}>
                        Order type: {order.order_type}
                      </Text>
                      <Text style={orderstyle.id}>
                        Sold By: {item.product.store_name}
                      </Text>
                      <Text style={orderstyle.date}>
                        Orderd on:{" "}
                        {moment(order.date).format("MMM Do YY, hh:mm a")}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <Text style={orderstyle.quantity}>
                          Quantity: {item.quantity}
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      alignItems: "flex-start",
                      flexDirection: "row",
                      margin: 6
                    }}
                  >
                    <Text style={orderstyle.quantity}>
                      &#8377;{item.quantity * item.product.price}
                    </Text>
                    <Text style={{ fontSize: 14 }}>Orderd status: </Text>
                    <Text>{item.order_status}</Text>

                    {item.order_message ? (
                      <Text>Order Message: {item.order_message}</Text>
                    ) : null}
                  </View>
                  <View
                    style={{
                      alignItems: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ order_status: "Confirmed" });
                        this.props.updateStatus(
                          this.props.order_details._id,
                          item.productId,
                          "Confirmed"
                        );
                      }}
                    >
                      <Text
                        style={{
                          backgroundColor:
                            item.order_status == "Confirmed"
                              ? "#f44f42"
                              : "#e1e1e1",
                          borderRadius: 10,
                          padding: 4,
                          margin: 5,
                          color:
                            item.order_status == "Confirmed" ? "#fff" : "#000"
                        }}
                      >
                        {item.order_status == "Confirmed" ? (
                          item.order_status
                        ) : (
                          "Confirm"
                        )}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ order_status: "Cancelled" });
                        this.props.updateStatus(
                          this.props.order_details._id,
                          item.productId,
                          "Cancelled"
                        );
                      }}
                    >
                      <Text
                        style={{
                          backgroundColor:
                            item.order_status == "Cancelled"
                              ? "#f44f42"
                              : "#e1e1e1",
                          borderRadius: 10,
                          padding: 4,
                          margin: 5,
                          color:
                            item.order_status == "Cancelled" ? "#fff" : "#000"
                        }}
                      >
                        {item.order_status == "Cancelled" ? (
                          item.order_status
                        ) : (
                          "Cancel"
                        )}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ order_status: "Dispatched" });
                        this.props.updateStatus(
                          this.props.order_details._id,
                          item.productId,
                          "Dispatched"
                        );
                      }}
                    >
                      <Text
                        style={{
                          backgroundColor:
                            item.order_status == "Dispatched"
                              ? "#f44f42"
                              : "#e1e1e1",
                          borderRadius: 10,
                          padding: 4,
                          margin: 5,
                          color:
                            item.order_status == "Dispatched" ? "#fff" : "#000"
                        }}
                      >
                        {item.order_status == "Dispatched" ? (
                          item.order_status
                        ) : (
                          "Dispatch"
                        )}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      margin: 5,
                      backgroundColor: "#f44242",
                      width: width,
                      height: 2
                    }}
                  />
                </View>
              ))
            ) : null}
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const orderstyle = {
  card: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 18,
    paddingTop: 16,
    paddingRight: 12,
    paddingBottom: 4,
    paddingLeft: 12,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 0,
    borderRadius: 9
  },
  image: {
    resizeMode: "contain",
    width: width / 4 + 30,
    height: 100,
    borderRadius: 3,
    overflow: "hidden"
  },
  name: {
    fontSize: 13,
    color: "#333",
    fontWeight: "600",
    paddingBottom: 5
  },
  id: {
    fontSize: 11,
    color: "#333",
    paddingBottom: 5
  },
  date: {
    fontSize: 10,
    color: "#333",
    paddingBottom: 5
  },
  quantity: {
    fontSize: 15,
    color: "#333",
    paddingBottom: 5,
    marginRight: 15
  },
  line: {
    borderTopWidth: 2,
    borderColor: "#f44242",
    width: width - 20,
    position: "relative",
    alignSelf: "center",
    marginTop: 15
  },
  fullline: {
    borderTopWidth: 2,
    borderColor: "#f44242",
    width: width - 20,
    position: "absolute",
    left: 0,
    marginTop: -2
  },
  icon: {
    flexDirection: "row",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "space-between",
    width: width - 60,
    top: -11,
    paddingTop: 6
  },
  horizontal: {
    flexDirection: "column",
    alignItems: "center",
    paddingTop: 3
  },
  round: {
    height: 16,
    width: 16,
    backgroundColor: "#f44242",
    borderRadius: 8,
    marginTop: -8
  },
  greyround: {
    height: 16,
    width: 16,
    backgroundColor: "#ddd",
    borderRadius: 8,
    marginTop: -8
  },
  halfgrey: {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: "#f44242",
    borderWidth: 6,
    borderColor: "#ddd",
    marginLeft: -2
  },
  ordertext: {
    fontSize: 10,
    fontWeight: "500",
    color: "#666",
    paddingTop: 8
  }
};

const mapStateToProps = state => {
  return state.Entities;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchOrderDetails: orderId =>
      dispatch(
        Request(
          { method: "GET", route: `orders/${orderId}`, name: "orderitem" },
          () => null
        )
      ),
    updateStatus: (orderId, productId, order_status) =>
      dispatch(
        Request(
          {
            method: "PUT",
            route: `orders/${orderId}`,
            name: "orderitem",
            params: { productId: productId, order_status: order_status }
          },
          () => null
        )
      )
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderInfo);
