import React from "react";
import { View, Image } from "react-native";

const IconSmall = props => {
  return (
    <View style={styles.container}>
      <Image style={[styles.icon, props.style]} source={props.img} />
    </View>
  );
};

export default IconSmall;

const styles = {
  container: {},
  icon: { height: 15, width: 15 }
};
