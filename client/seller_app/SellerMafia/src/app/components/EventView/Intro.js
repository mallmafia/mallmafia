import React, { Component } from "react";
import { View, Text, ActivityIndicator } from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class Intro extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size={40} />
      </View>
    );
  }
}

export default Intro;

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  }
};
