import React, { Component } from "react";
import { View, Modal } from "react-native";
import Spin from "react-native-spinkit";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ON_LOAD, OFF_LOAD } from "@data/constants";

class Spinner extends Component {
  render() {
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.spinner}
        onRequestClose={() => this.props.onBack()}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <Spin type={"FadingCircleAlt"} color={"#fff"} size={50} />
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToprops = state => {
  return state.EventState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { onBack: () => dispatch({ type: OFF_LOAD }) },
    dispatch
  );
};

export default connect(mapStateToprops, mapDispatchToProps)(Spinner);

const styles = {
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "relative",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  content: {
    height: 60,
    width: 60,
    borderRadius: 5,
    // backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
};
