import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  Dimensions
} from "react-native";

import AppEventEmitter from "@utils/AppEventEmitter";

const { width, height, scale } = Dimensions.get("window");

class Header extends Component {
  open() {
    AppEventEmitter.emit("hamburger.click");
  }

  render() {
    const { containerStyle } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <StatusBar backgroundColor="#ba001a" />
        <TouchableOpacity onPress={this.open}>
          <View style={styles.menuIconView}>
            <Image
              source={require("@images/icon-nav.png")}
              style={styles.menuIcon}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Header;

const styles = {
  container: {
    width: width,
    position: "absolute",
    top: 0,
    height: 50,
    backgroundColor: "#f44242",
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 15
  },
  menuIconView: {
    backgroundColor: "rgba(0,0,0,0.05)",
    paddingLeft: 12,
    paddingRight: 16,
    borderBottomRightRadius: 25,
    borderTopRightRadius: 25,
    justifyContent: "center",
    height: 50
  },
  menuIcon: {
    height: 17,
    width: 17,
    tintColor: "#fff"
  }
};
