import React from "react";
import { View, Text } from "react-native";

const Card = props => {
  return (
    <View style={styles.container}>
      {props.children}
    </View>
  );
};

export default Card;

const styles = {
  container: {
    flex: 1,
    marginTop: 5,
    marginLeft: 4,
    marginRight: 4,
    borderRadius: 2,
    padding: 4,
    backgroundColor: "#fff",
    elevation: 2
  }
};
