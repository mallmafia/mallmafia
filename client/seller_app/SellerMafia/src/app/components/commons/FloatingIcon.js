import React, { Component } from "react";
import { TouchableOpacity, Image } from "react-native";

class FloatingIcon extends Component {
  render() {
    const { onPress, image } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image style={styles.icon} source={image} />
      </TouchableOpacity>
    );
  }
}

export default FloatingIcon;

const styles = {
  container: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    bottom: 40,
    right: 20,
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: "#f44242",
    elevation: 5
  },
  icon: {
    height: 25,
    width: 25,
    tintColor: "#fff"
  }
};
