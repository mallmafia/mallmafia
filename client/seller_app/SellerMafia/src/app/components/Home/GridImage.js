import React from "react";
import { View, Image } from "react-native";

const GridImage = props => {
  const { image } = props;
  return <Image style={styles.container} source={image} />;
};

export default GridImage;

const styles = {
  container: {
    height: 50,
    width: 50,
    tintColor: "#f00"
  }
};
