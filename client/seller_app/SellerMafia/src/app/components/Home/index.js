import React, { Component } from "react";
import { View, Text } from "react-native";

import Header from "@commons/Header";
import Grid from "./Grid";

import { Spinner } from "../EventView";

import { elements } from "./home";

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Grid items={elements} />
        <Header name={"Home"} />
        <Spinner />
      </View>
    );
  }
}

export default Home;

styles = {
  container: { flex: 1, backgroundColor: "#fff", paddingTop: 50 }
};
