import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions
} from "react-native";

const { width, height, scale } = Dimensions.get("window");

import Badge from "./Badge";
import GridImage from "./GridImage";

import { GetActions } from "@data/functions";

class Grid extends Component {
  render() {
    const { items } = this.props;
    return (
      <ScrollView>
        <View style={styles.container}>
          {items.map((d, i) => (
            <TouchableOpacity
              onPress={() => GetActions({ toScene: d.toScene })}
              key={i}
              style={styles.grid.container}
            >
              <View style={styles.grid.element}>
                <GridImage key={i} image={d.icon} />
              </View>
              {d.badge ? (
                <Badge data={d.badge} style={styles.grid.badge} />
              ) : null}
              <Text style={styles.grid.label}>{d.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    );
  }
}
export default Grid;

const styles = {
  container: {
    flexWrap: "wrap",
    flexDirection: "row",
    backgroundColor: "#ddd",
    padding: 4
  },
  grid: {
    container: {
      height: width / 2 - 8,
      width: width / 2 - 8,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#fff",
      borderRadius: 5,
      margin: 2
    },
    element: {
      height: width / 4,
      width: width / 4,
      borderRadius: width / 8,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#eee"
    },
    label: {
      position: "absolute",
      bottom: 5,
      paddingTop: 5,
      color: "#000"
    },
    badge: {
      right: 15,
      top: 15
    }
  }
};
