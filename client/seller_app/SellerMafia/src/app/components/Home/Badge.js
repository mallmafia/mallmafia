import React, { Component } from "react";
import { View, Text, Dimensions, Animated, Easing } from "react-native";

const { width, height, scale } = Dimensions.get("window");

class Batch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radius: 40,
      animatedValue: new Animated.Value(0)
    };
  }

  componentDidMount() {
    Animated.timing(this.state.animatedValue, {
      toValue: 1,
      easing: Easing.bounce,
      duration: 600
    }).start();
  }

  render() {
    const { data, style } = this.props;
    const anm_styles = {
      ...styles.container,
      height: this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, this.state.radius]
      }),
      width: this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, this.state.radius]
      }),
      borderRadius: this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, this.state.radius]
      })
    };

    return (
      <Animated.View style={[anm_styles, style]}>
        <Text style={styles.text}>
          {data.txt}
        </Text>
      </Animated.View>
    );
  }
}
export default Batch;

const styles = {
  container: {
    marginRight: 10,
    marginTop: 10,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f57f17"
  },
  text: {
    color: "#fff"
  }
};
