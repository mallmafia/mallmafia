import React from "react";
import { View, TouchableOpacity, Text, Dimensions } from "react-native";

const { width, height, scale } = Dimensions.get("window");

import GridImage from "./GridImage";

const GridItem = props => {
  const { data } = props;
  return (
    <TouchableOpacity onPress={() => null} style={styles.container}>
      <GridImage image={{ uri: data.showcase }} />
      <Text style={styles.label}>{data.name}</Text>
    </TouchableOpacity>
  );
};

export default GridItem;

const styles = {
  container: {
    height: width / 2 - 6,
    width: width / 2 - 6,
    backgroundColor: "#fff",
    borderRadius: 5,
    margin: 1,
    padding: 2
  },
  label: {
    position: "absolute",
    bottom: 5,
    paddingTop: 5,
    color: "#000"
  }
};
