import React, { Component } from "react";
import { View, Text, ListView, TouchableOpacity } from "react-native";

import GridItem from "./GridItem";

class ProductGrid extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      listSource: ds.cloneWithRows(this.props.items)
    };
  }

  render() {
    const { items } = this.props;
    return (
      <View style={styles.container}>
        <ListView
          contentContainerStyle={styles.list}
          dataSource={this.state.listSource}
          renderRow={d => <GridItem data={d} />}
        />
      </View>
    );
  }
}
export default ProductGrid;

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ddd",
    padding: 4
  },
  list: {
    flexDirection: "row",
    flexWrap: "wrap"
  }
};
