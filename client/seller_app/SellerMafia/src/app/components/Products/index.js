import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import { Actions } from "react-native-router-flux";

import { img } from "./imgs";
import { products } from "@mock/products";

import FloatingIcon from "@commons/FloatingIcon";
import ProductGrid from "./ProductGrid";
import ProductHeader from "./ProductHeader";

class Products extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ProductGrid items={products} />
        <FloatingIcon image={{ uri: img.icon.add }} onPress={Actions.add} />
        <ProductHeader />
      </View>
    );
  }
}

export default Products;

const styles = {
  container: {
    flex: 1,
    paddingTop: 50
  }
};
