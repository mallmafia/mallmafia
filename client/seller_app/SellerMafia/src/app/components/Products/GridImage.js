import React from "react";
import { View, Image } from "react-native";

const GridImage = props => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={props.image} />
    </View>
  );
};

export default GridImage;

const styles = {
  container: {
    backgroundColor: "#f2f2f2",
    alignItems: "center"
  },
  image: {
    height: 100,
    width: 100,
    resizeMode: "center"
  }
};
