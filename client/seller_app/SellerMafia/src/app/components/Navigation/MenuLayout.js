import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image, ScrollView } from "react-native";
import css from "./style";

import { Actions } from "react-native-router-flux";

import { Logout } from "@data/functions";

export default class SideMenu extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[css.sideMenu, this.props.menuBody]}>
          <View style={css.profile}>
            <Image
              style={css.avatar}
              source={{
                uri:
                  "https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png"
              }}
            />
            <Text style={[css.fullname, this.props.textColor]}>Minh Pham</Text>
            <Text style={[css.email, this.props.textColor]}>
              minh@beostore.io
            </Text>
          </View>

          <TouchableOpacity
            style={[css.menuRow, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.wooProduct}
          >
            <Text style={[css.menuLink, this.props.textColor]}>Shop</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[css.menuRow, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.news}
          >
            <Text style={[css.menuLink, this.props.textColor]}>News</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[css.menuRow, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.templates}
          >
            <Text style={[css.menuLink, this.props.textColor]}>Templates</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[css.menuRow, css.menuSignOut, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Logout}
          >
            <Text style={[css.menuLink, css.logoutLink, this.props.textColor]}>
              Logout
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
