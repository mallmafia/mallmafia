"use strict";
import React, { Component } from "react";
import AppEventEmitter from "@utils/AppEventEmitter";
import { Router } from "react-native-router-flux";
import Drawer from "react-native-drawer";
import MenuLayout from "./MenuLayout";
import css from "./style";

export default class Menu extends Component {
  componentDidMount() {
    AppEventEmitter.addListener(
      "hamburger.click",
      this.openSideMenu.bind(this)
    );
  }

  closeSideMenu() {
    if (typeof this.refs.drawer != "undefined") {
      this.refs.drawer.close();
    }
  }
  openSideMenu() {
    this.refs.drawer.open();
  }

  render() {
    const drawerStyles = {
      drawer: { backgroundColor: "rgba(0, 0, 0, 0.2)" }
    };
    return (
      <Drawer
        ref="drawer"
        type="overlay"
        tapToClose={true}
        panCloseMask={0.1}
        openDrawerOffset={0.1}
        styles={drawerStyles}
        content={
          <MenuLayout
            textColor={{ color: "#fff" }}
            rowStyle={{ borderTopWidth: 0 }}
            menuBody={css.menuOverlay}
          />
        }
      >
        <Router
          dispatch={this.closeSideMenu.bind(this)}
          hideNavBar={true}
          scenes={this.props.scenes}
        />
      </Drawer>
    );
  }
}
