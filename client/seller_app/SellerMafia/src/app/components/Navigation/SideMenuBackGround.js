import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";
import css from "./style";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/Ionicons";
import { AsyncSet, AsyncGet } from "@utils";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Fetch, Flush } from "@data/actions";
class SideMenuIcons extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[css.sideMenuLeft, this.props.menuBody]}>
          <Image
            source={require("@images/menubackground.png")}
            style={css.menuBg}
          />
          <TouchableOpacity
            style={[css.menuRowLeft, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.market}
          >
            <Icon
              name={"ios-cart-outline"}
              style={[css.icon, this.props.iconStyle]}
            />
            <Text style={[css.menuLinkLeft, this.props.textColor]}>Orders</Text>

            <View style={css.badge}>
              <Image
                source={require("@images/circle.png")}
                style={css.badgeIcon}
              >
                <Text style={css.badgeText}>0</Text>
              </Image>
            </View>
          </TouchableOpacity>

          {/* <TouchableOpacity
            style={[css.menuRowLeft, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.auth}
          >
            <Icon name={"ios-lock"} style={[css.icon, this.props.iconStyle]} />
            <Text style={[css.menuLinkLeft, this.props.textColor]}>Auth</Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            style={[css.menuRowLeft, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.profile}
          >
            <Icon
              name={"ios-person"}
              style={[css.icon, this.props.iconStyle]}
            />
            <Text style={[css.menuLinkLeft, this.props.textColor]}>
              Profile
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[css.menuRowLeft, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={Actions.map}
          >
            <Icon
              name={"ios-settings-outline"}
              style={[css.icon, this.props.iconStyle]}
            />
            <Text style={[css.menuLinkLeft, this.props.textColor]}>NavMap</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[css.menuRowLeft, css.menuSignOut, this.props.rowStyle]}
            underlayColor="#2D2D30"
            onPress={() => {
              AsyncStorage.removeItem("@jwtkey", () => null);
              Actions.auth({ type: "reset" });
              this.props.Flush({ name: "user" });
            }}
          >
            <Icon
              name={"ios-log-out-outline"}
              style={[css.icon, this.props.iconStyle]}
            />
            <Text
              style={[
                css.menuLinkLeft,
                css.logoutLinkLeft,
                this.props.textColor
              ]}
            >
              Logout
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
const mapStateToprops = state => {
  if (state.FetchState.hasOwnProperty("user")) return state.FetchState.user;
  else return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Fetch: Fetch, Flush: Flush }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(SideMenuIcons);
