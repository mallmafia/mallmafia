import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar
} from "react-native";
const { width, height } = Dimensions.get("window");

import Camera from "react-native-camera";

import { ADD_CAMERA_IMAGE } from "@data/constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class LineInput extends Component {
  takePicture() {
    const options = {};
    this.camera
      .capture({ metadata: options })
      .then(data => this.props.addImg(data))
      .catch(err => console.error(err));
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Camera
          ref={cam => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}
        >
          <TouchableOpacity
            style={styles.button}
            onPress={this.takePicture.bind(this)}
          />
        </Camera>
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.UtilsState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { addImg: data => ({ type: ADD_CAMERA_IMAGE, payload: data }) },
    dispatch
  );
};

export default connect(mapStateToprops, mapDispatchToProps)(LineInput);

const styles = {
  container: {
    height: height,
    width: width
  },
  preview: {
    flex: 1,
    alignItems: "center"
  },
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    position: "absolute",
    bottom: 80,
    backgroundColor: "#fff",
    borderWidth: 2,
    borderColor: "#f00"
  }
};
