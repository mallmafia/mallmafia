import React, { Component } from "react";
import { Navigator, Text, View } from "react-native";
import { Scene, Actions } from "react-native-router-flux";
import Menu from "./Navigation";
import AppEventEmitter from "@utils/AppEventEmitter";

import Home from "./Home";
import Auth from "./Auth/Auth";
import Intro from "./EventView/Intro";
import Orders from "./Orders";
import OrderInfo from "./Orders/OrderInfo";
import Products from "./Products";
import AddProducts from "./AddProducts";
import CameraView from "./CameraView";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Check } from "@data/actions";

class RootRouter extends Component {
  constructor(props) {
    super(props);
    this.state = { menuStyle: 0 };
  }

  componentWillMount() {
    this.props.Check(
      { method: "get", route: "seller", name: "user" },
      () => Actions.home({ type: "reset" }),
      () => Actions.auth({ type: "reset" })
    );
  }

  changeMenuStyle(data) {
    this.setState({ menuStyle: data.menuId });
  }

  componentDidMount() {
    AppEventEmitter.addListener(
      "app.changeMenuStyle",
      this.changeMenuStyle.bind(this)
    );
  }

  render() {
    const scenes = Actions.create(
      <Scene key="scene">
        <Scene key="intro" component={Intro} hideNavBar={true} />
        <Scene key="auth" component={Auth} hideNavBar={true} />
        <Scene key="home" component={Home} hideNavBar={true} />
        <Scene key="orders" component={Orders} hideNavBar={true} />
        <Scene key="orderinfo" component={OrderInfo} hideNavBar={true} />
        <Scene key="products" component={Products} hideNavBar={true} />
        <Scene key="add" component={AddProducts} hideNavBar={true} />
        <Scene key="camera" component={CameraView} hideNavBar={true} />
      </Scene>
    );

    return <Menu ref="menuDefault" scenes={scenes} />;
  }
}

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Check: Check }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(RootRouter);
