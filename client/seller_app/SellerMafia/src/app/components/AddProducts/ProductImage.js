import React, { Component } from "react";
import { View, Text, Image, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

class ProductImage extends Component {
  render() {
    const { images } = this.props;
    return (
      <View style={styles.container}>
        {images.map((d, i) => (
          <Image style={styles.image} key={i} source={{ uri: d.path }} />
        ))}
      </View>
    );
  }
}

export default ProductImage;

const styles = {
  container: {
    flexWrap: "wrap",
    flexDirection: "row"
  },
  image: {
    height: width / 2,
    width: width / 2 - 10,
    margin: 5,
    resizeMode: "contain"
  }
};
