import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";

class LineInput extends Component {
  render() {
    const { label } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{label}</Text>
        <TextInput
          style={styles.textInput}
          underlineColorAndroid={"rgba(0,0,0,0.1)"}
          {...this.props}
        />
      </View>
    );
  }
}

export default LineInput;

const styles = {
  container: {
    padding: 10
  },
  label: {
    paddingLeft: 5,
    color: "#000"
  },
  textInput: {}
};
