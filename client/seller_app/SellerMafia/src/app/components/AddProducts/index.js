import React, { Component } from "react";
import { View, ScrollView, Text, Image, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import AddProductHeader from "./AddProductHeader";
import LineInput from "./LineInput";
import ProductImage from "./ProductImage";

class AddProducts extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { camImage } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          <LineInput label={"Name"} placeholder={"your name"} />
          <LineInput label={"Quantity"} placeholder={"10"} />
          <LineInput label={"Price"} placeholder={"1000"} />
          {camImage.length !== 0 ? <ProductImage images={camImage} /> : null}
          <TouchableOpacity style={styles.camButton} onPress={Actions.camera}>
            <Text>Add photo</Text>
          </TouchableOpacity>
        </ScrollView>
        <AddProductHeader />
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.UtilsState;
};

export default connect(mapStateToprops, {})(AddProducts);

const styles = {
  container: {
    flex: 1,
    paddingTop: 50
  },
  camButton: {
    height: 40,
    borderRadius: 5,
    backgroundColor: "#f44242",
    alignItems: "center",
    justifyContent: "center"
  }
};
