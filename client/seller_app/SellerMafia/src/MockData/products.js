export const products = [
  {
    name: "Puma Shoe",
    rating: 3,
    description:
      "Puma is one of the world's leading sports brands, designing, developing, selling and marketing footwear, apparel and accessories. For over 65 years, Puma has established a history of making fast product designs for the fastest athletes on the planet. Puma offers performance and sport-inspired lifestyle products in categories such as Football, Running, Training and Fitness, Golf, and MoGermany.",
    price: 3400,
    store_id: 1,
    store_name: "Puma Official",
    mall_id: 1,
    category_id: 3,
    quantity: 50,
    tags: ["Puma", "Shoes", "Fashion", "Red"],
    reviews: [],
    showcase:
      "https://ae01.alicdn.com/kf/HTB1Hm6qPpXXXXXxXXXXq6xXFXXXE/Original-New-Arrival-2017-font-b-PUMA-b-font-Disc-Blaze-Unisex-Skateboarding-font-b-Shoes.jpg",
    imgs: [
      "https://images-na.ssl-images-amazon.com/images/I/81unUrR7w%2BL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81KvNmes7QL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81MxABniC1L._UX395_.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp4.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp5.jpg"
    ]
  },
  {
    name: "Puma Shoe",
    rating: 3,
    description:
      "Puma is one of the world's leading sports brands, designing, developing, selling and marketing footwear, apparel and accessories. For over 65 years, Puma has established a history of making fast product designs for the fastest athletes on the planet. Puma offers performance and sport-inspired lifestyle products in categories such as Football, Running, Training and Fitness, Golf, and MoGermany.",
    price: 3400,
    store_id: 1,
    store_name: "Puma Official",
    mall_id: 1,
    category_id: 3,
    quantity: 50,
    tags: ["Puma", "Shoes", "Fashion", "Red"],
    reviews: [],
    showcase:
      "http://i9.offers.gallery/p-36-70-3670f3943206229346b6e339a89b04fa500x500/pozostale-plecaki-adidas-originals-plecak-mottled-grey-4056564701521.jpg",
    imgs: [
      "https://images-na.ssl-images-amazon.com/images/I/81unUrR7w%2BL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81KvNmes7QL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81MxABniC1L._UX395_.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp4.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp5.jpg"
    ]
  },
  {
    name: "Puma Shoe",
    rating: 3,
    description:
      "Puma is one of the world's leading sports brands, designing, developing, selling and marketing footwear, apparel and accessories. For over 65 years, Puma has established a history of making fast product designs for the fastest athletes on the planet. Puma offers performance and sport-inspired lifestyle products in categories such as Football, Running, Training and Fitness, Golf, and MoGermany.",
    price: 3400,
    store_id: 1,
    store_name: "Puma Official",
    mall_id: 1,
    category_id: 3,
    quantity: 50,
    tags: ["Puma", "Shoes", "Fashion", "Red"],
    reviews: [],
    showcase:
      "http://cdn-media.mophie.com/shop/media/catalog/product/cache/3/image/9df78eab33525d08d6e5fb8d27136e95/j/p/jp-ssg5-blk_front-back-left-3qtr_2000px.jpg",
    imgs: [
      "https://images-na.ssl-images-amazon.com/images/I/81unUrR7w%2BL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81KvNmes7QL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81MxABniC1L._UX395_.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp4.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp5.jpg"
    ]
  },
  {
    name: "Puma Shoe",
    rating: 3,
    description:
      "Puma is one of the world's leading sports brands, designing, developing, selling and marketing footwear, apparel and accessories. For over 65 years, Puma has established a history of making fast product designs for the fastest athletes on the planet. Puma offers performance and sport-inspired lifestyle products in categories such as Football, Running, Training and Fitness, Golf, and MoGermany.",
    price: 3400,
    store_id: 1,
    store_name: "Puma Official",
    mall_id: 1,
    category_id: 3,
    quantity: 50,
    tags: ["Puma", "Shoes", "Fashion", "Red"],
    reviews: [],
    showcase:
      "http://cdn-media.mophie.com/shop/media/catalog/product/cache/3/image/9df78eab33525d08d6e5fb8d27136e95/j/p/jp-ssg5-blk_front-back-left-3qtr_2000px.jpg",
    imgs: [
      "https://images-na.ssl-images-amazon.com/images/I/81unUrR7w%2BL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81KvNmes7QL._UL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/81MxABniC1L._UX395_.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp4.jpg",
      "http://mallmafia.herokuapp.com/sites/products/tp5.jpg"
    ]
  }
];
