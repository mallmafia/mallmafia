import { URL } from "../../env.js";

export const API_URL = URL;

export const LOGIN_SUCCESS = "login_user_success";
export const LOGIN_FAIL = "login_user_fail";

export const REGISTER_SUCCESS = "register_user_success";
export const REGISTER_FAIL = "register_user_fail";
export const MISSING_CREDENTIALS = "missing_credentials";

export const FETCH_SUCCESS = "fetch_data_success";
export const FETCH_FAIL = "fetch_data_fail";
export const FLUSH_DATA = "flush_data";

export const ON_LOAD = "on_app_loads";
export const OFF_LOAD = "on_app_load_stops";

export const ON_DROP = "dropdown_msg_show";
export const OFF_DROP = "dropdown_msg_close";

export const ON_ALERT = "pop_alert_box";
export const OFF_ALERT = "close_alert_box";

export const ADD_CAMERA_IMAGE = "add_path_to_camera_image";

export const NULL = "null";
