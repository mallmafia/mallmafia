import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import Thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import {
  AuthReducer,
  FetchReducer,
  EventReducer,
  UtilsReducer,
  NetworkReducer
} from "./reducers";

const reducers = combineReducers({
  AuthState: AuthReducer,
  FetchState: FetchReducer,
  EventState: EventReducer,
  UtilsState: UtilsReducer,
  Entities: NetworkReducer
});

const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000 });
const store = createStore(
  reducers,
  {},
  composeEnhancers(applyMiddleware(Thunk))
);

export default store;
