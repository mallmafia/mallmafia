import { FLUSH_DATA, ON_LOAD, OFF_LOAD } from "../constants";

export const Flush = (params, callback) => {
  return dispatch => {
    dispatch({ type: ON_LOAD });
    dispatch({ type: FLUSH_DATA, payload: { name: params.name } });
    dispatch({ type: OFF_LOAD });
    callback();
  };
};
