import { Actions } from "react-native-router-flux";

import {
  API_URL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  MISSING_CREDENTIALS,
  NULL,
  ON_ALERT,
  OFF_ALERT,
  ON_LOAD,
  OFF_LOAD
} from "../constants";
var type = NULL;

import { Fetch } from "./fetch";

import { AsyncSet } from "@utils/AsyncSet";

export const Login = params => {
  const { email, password } = params;
  const data = { email: email, password: password };
  return dispatch => {
    if (email && password) {
      const json = JSON.stringify(data);
      dispatch({ type: ON_LOAD });
      fetch(`${API_URL}/sign_in`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: json
      })
        .then(response => response.json())
        .then(response => {
          HandleSuccessResponse(dispatch, response);
        })
        .catch(error => {
          HandleFailResponse(dispatch, error);
        })
        .done(() => dispatch({ type: OFF_LOAD }));
    } else {
      HandleParamsMismatch(dispatch, data);
    }
  };
};

const HandleSuccessResponse = (dispatch, res) => {
  if (res.status == 200) {
    type = LOGIN_SUCCESS;
    AsyncSet({ key: "@jwtkey", data: res.token }, () => null);
    dispatch({
      type: ON_ALERT,
      payload: {
        text: "Login success",
        buttons: [
          {
            label: "ok",
            onPress: () => {
              dispatch({ type: OFF_ALERT });
              return Actions.home({ type: "reset" });
            }
          }
        ]
      }
    });
    dispatch(
      Fetch({ method: "get", route: "seller", name: "user" }, data => null)
    );
  } else {
    return HandleFailResponse(dispatch, res.message);
  }
  return dispatch({ type: type, payload: res.token });
};

const HandleFailResponse = (dispatch, err) => {
  if (err) {
    type = LOGIN_FAIL;
    dispatch({
      type: ON_ALERT,
      payload: {
        text: err,
        buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
      }
    });
  }
  return dispatch({ type: type, payload: err });
};

const HandleParamsMismatch = (dispatch, data) => {
  var msg = null;
  type = MISSING_CREDENTIALS;
  if (data.email == "") {
    msg = "Enter a email address";
  } else if (data.password == "") {
    msg = "Enter password";
  }
  dispatch({
    type: ON_ALERT,
    payload: {
      text: msg,
      buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
    }
  });
  return dispatch({ type: type, payload: msg });
};
