import {
  API_URL,
  NULL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  MISSING_CREDENTIALS,
  ON_LOAD,
  OFF_LOAD,
  ON_ALERT,
  OFF_ALERT
} from "../constants";
var type = NULL;

export const Register = params => {
  const { name, email, phone, password } = params;
  return dispatch => {
    const data = {
      fullName: name,
      email: email,
      phone: phone,
      password: password
    };
    if (name && phone && password && email) {
      const json = JSON.stringify(data);
      dispatch({ type: ON_LOAD });
      fetch(`${API_URL}/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: json
      })
        .then(response => response.json())
        .then(response => {
          HandleSuccessResponse(dispatch, response);
        })
        .catch(error => {
          HandleFailResponse(dispatch, error);
        })
        .done(() => dispatch({ type: OFF_LOAD }));
    } else {
      HandleParamsMismatch(dispatch, data);
    }
  };
};

const HandleSuccessResponse = (dispatch, res) => {
  console.log(res);
  if (res.status == 200) {
    type = REGISTER_SUCCESS;
    dispatch({
      type: ON_ALERT,
      payload: {
        text: "Registration successful",
        buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
      }
    });
  } else {
    return HandleFailResponse(dispatch, res.message);
  }
  return dispatch({ type: type, payload: res.results });
};

const HandleFailResponse = (dispatch, err) => {
  if (err) {
    type = REGISTER_FAIL;
    dispatch({
      type: ON_ALERT,
      payload: {
        text: err.errmsg,
        buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
      }
    });
  }
  return dispatch({ type: type, payload: err });
};

const HandleParamsMismatch = (dispatch, data) => {
  var msg = null;
  type = MISSING_CREDENTIALS;
  if (data.fullName == "") {
    msg = "Enter full name";
  } else if (data.email == "") {
    msg = "Enter a valid email";
  } else if (data.phone == "") {
    msg = "Enter a valid phone number";
  } else if (data.password == "") {
    msg = "Enter password";
  }
  dispatch({
    type: ON_ALERT,
    payload: {
      text: msg,
      buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
    }
  });
  return dispatch({ type: type, payload: msg });
};
