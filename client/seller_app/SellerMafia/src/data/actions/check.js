import { FETCH_SUCCESS, FETCH_FAIL, API_URL, NULL } from "../constants";
import store from "../store";
import { AsyncStorage } from "react-native";
var type = NULL;

export const Check = (params, callback, err_callback) => {
  return dispatch => {
    AsyncStorage.getItem("@jwtkey", (err, key) => {
      var method = setMethod(params.method);
      var route = params.route;
      var token = key;
      var body = params.params ? params.params : null;
      var options = {
        method: method,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `JWT ${JSON.parse(key)}`
        }
      };

      if (body) {
        options["body"] = JSON.stringify(body);
      }
      fetch(`${API_URL}/${route}`, options)
        .then(res => res.json())
        .then(res => {
          HandleSuccessResponse(
            dispatch,
            params.name,
            res,
            callback,
            err_callback
          );
        })
        .catch(error => {
          HandleFailResponse(dispatch, error);
        })
        .done();
    });
  };
};

const setMethod = m => {
  if (m == "POST" || m == "post") {
    return "POST";
  } else if (m == "GET" || m == "get") {
    return "GET";
  } else if (m == "PUT" || m == "put") {
    return "PUT";
  } else if (m == "DELETE" || m == "delete") {
    return "DELETE";
  }
  return "GET";
};

const getToken = () => {
  var token = store.getState().AuthState.jwtkey;
  if (token) {
    return token;
  }
  return null;
};

const HandleSuccessResponse = (
  dispatch,
  name,
  data,
  callback,
  err_callback
) => {
  if (data) {
    type = FETCH_SUCCESS;
    callback(data);
  } else {
    err_callback();
  }
  return dispatch({ type: type, payload: { name: name, data: data } });
};

const HandleFailResponse = (dispatch, err) => {
  if (err) {
    type = FETCH_FAIL;
  }
  return dispatch({ type: type, payload: err });
};
