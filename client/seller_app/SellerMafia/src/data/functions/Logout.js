import { Actions } from "react-native-router-flux";
import { AsyncStorage } from "react-native";

import store from "../store";

import { Flush } from "../actions";

export const Logout = () => {
  AsyncStorage.removeItem("@jwtkey", () => null);
  store.dispatch(
    Flush({ name: "user" }, () => Actions.auth({ type: "reset" }))
  );
};
