export const toCamelCase = str => {
  let camelCase = str.replace(/-([a-z])|_([a-z])/g, t => t.toUpperCase());
  return camelCase;
};
