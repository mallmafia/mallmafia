import { Actions } from "react-native-router-flux";

export const GetActions = params => {
  switch (params.toScene.toLowerCase()) {
    case "orders":
      return Actions.orders();
    case "products":
      return Actions.products();
    default:
      return null;
  }
};
