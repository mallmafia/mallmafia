import AuthReducer from "./AuthReducer";
import FetchReducer from "./FetchReducer";
import EventReducer from "./EventReducer";
import UtilsReducer from "./UtilsReducer";
import NetworkReducer from "./NetworkReducer";

export {
  AuthReducer,
  FetchReducer,
  EventReducer,
  UtilsReducer,
  NetworkReducer
};
