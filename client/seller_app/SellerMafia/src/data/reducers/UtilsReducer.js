import { ADD_CAMERA_IMAGE } from "../constants";

const InitialState = {
  camImage: []
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case ADD_CAMERA_IMAGE:
      return { ...state, camImage: [...state.camImage, action.payload] };
    default:
      return { ...state };
  }
};
