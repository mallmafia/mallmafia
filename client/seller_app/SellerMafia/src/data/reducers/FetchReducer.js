import { FETCH_SUCCESS, FETCH_FAIL, NULL, FLUSH_DATA } from "../constants";

const InitialState = {
  entities: {}
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.payload.name]: action.payload.data
        }
      };
    case FETCH_FAIL:
      return { ...state, error: action.payload };
    case FLUSH_DATA:
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.payload.name]: null
        }
      };
    case NULL:
      return { ...state };
    default:
      return state;
  }
};
