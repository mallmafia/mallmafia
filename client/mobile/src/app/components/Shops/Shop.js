import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet
} from "react-native";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
const { width, height, scale } = Dimensions.get("window");
import Header from "@commons/components/Header";
import StarRating from "@commons/components/StarRating";
import { connect } from "react-redux";
import { Fetch } from "@data/actions";

var styles = StyleSheet.create({
  sampleStyle: {
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: "red"
  }
});
class Shop extends Component {
  componentDidMount() {
    this.props.fetchProducts();
  }
  render() {
    if (!this.props.entities.seller_products) {
      this.props.fetchProducts();
      return <View />;
    }
    return (
      <View style={{ flex: 1 }}>
        <Header name={this.props.shop.name} searchButton cartButton static />
        <ScrollView>
          <View
            style={{
              height: height / 4,
              flexDirection: "row",
              backgroundColor: "#eee"
            }}
          >
            <Image
              source={{ uri: this.props.shop.showcase }}
              style={{
                width: width / 3 - 10,
                height: width / 3,
                top: 0,
                borderRadius: 23,
                margin: 10
              }}
            />
            <View>
              <View
                style={{
                  flexDirection: "row",
                  marginRight: 20,
                  marginTop: 20
                }}
              >
                <Icon
                  name="store"
                  style={{
                    position: "relative",
                    fontSize: 30,
                    color: "#f44242"
                  }}
                />
                <Text
                  style={{ fontFamily: "sans-serif-condensed", fontSize: 23 }}
                >
                  {this.props.shop.name.slice(0, 20)}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginRight: 20,
                  marginTop: 5
                }}
              >
                <Icon
                  name="map-marker"
                  style={{
                    position: "relative",
                    fontSize: 20,
                    color: "#f44242"
                  }}
                />
                <Text
                  style={{ fontFamily: "sans-serif-condensed", fontSize: 15 }}
                >
                  Brookfields, Coimbatore
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginRight: 20,
                  marginTop: 5
                }}
              >
                <Icon
                  name="tag"
                  style={{
                    position: "relative",
                    fontSize: 20,
                    color: "#f44242"
                  }}
                />
                <Text
                  style={{ fontFamily: "sans-serif-condensed", fontSize: 15 }}
                >
                  {this.props.shop.tags.join(", ")}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginRight: 20,
                  marginTop: 5,
                  justifyContent: "space-between",
                  width: width / 2 + 20
                }}
              >
                <StarRating
                  disabled={false}
                  maxStars={5}
                  rating={this.props.shop.rating}
                  starColor={"#f44242"}
                  starSize={18}
                />
                <View
                  style={{
                    borderColor: "#f44242",
                    borderWidth: 1,
                    borderRadius: 5,
                    backgroundColor: "#f44242"
                  }}
                >
                  <Text
                    style={{ margin: 5, fontWeight: "normal", color: "#fff" }}
                  >
                    Contact
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{ margin: 8 }}>
            <Text style={{ fontSize: 18 }}>Products</Text>
            <View style={{ backgroundColor: "#f44242", height: 2, width: 80 }}>
              {}
            </View>
            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap"
              }}
            >
              {this.props.entities.seller_products.map(seller_product => (
                <TouchableOpacity
                  style={{ marginBottom: 10 }}
                  onPress={() => Actions.product({ product: seller_product })}
                >
                  <View style={{ alignItems: "center" }}>
                    <Text style={{ fontSize: 16 }}>
                      {seller_product.name.slice(0, 10)}{" "}
                    </Text>
                    <Image
                      source={{ uri: seller_product.showcase }}
                      style={{
                        width: width / 3 + 50,
                        height: height / 3,
                        resizeMode: "contain"
                      }}
                    />
                    <Text
                      style={{ justifyContent: "center", fontWeight: "500" }}
                    >
                      {" "}
                      &#8377; {seller_product.price}
                    </Text>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      rating={this.props.shop.rating}
                      starColor={"#f44242"}
                      starSize={18}
                    />
                  </View>
                  <View
                    style={{
                      margin: 5,
                      backgroundColor: "#f44242",
                      width: width / 3 + 30,
                      height: 2
                    }}
                  />
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchProducts: () =>
      dispatch(
        Fetch(
          {
            method: "GET",
            route: `product?ids=${ownProps.shop.product_ids.join()}`,
            name: "seller_products"
          },
          data => null
        )
      )
  };
};
export default connect(mapStateToprops, mapDispatchToProps)(Shop);
