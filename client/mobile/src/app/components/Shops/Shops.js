import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image
} from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
import StarRating from "@commons/components/StarRating";
import Header from "@commons/components/Header";
export default class Shops extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="Stores" searchButton cartButton static />
        <ScrollView>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap"
            }}
          >
            {this.props.stores.map(store => (
              <TouchableOpacity
                style={{ margin: 5, marginBottom: 10 }}
                onPress={() => Actions.store({ shop: store })}
              >
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={{ uri: store.showcase }}
                    style={{
                      width: width / 3 + 50,
                      height: height / 3,
                      resizeMode: "contain"
                    }}
                  />
                  <Text style={{ fontSize: 16 }}>{store.name} </Text>
                  <Text style={{ fontFamily: "sans-serif-condensed" }}>
                    {store.tags.join()}
                  </Text>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={store.rating}
                    starColor={"#f44242"}
                    starSize={18}
                  />
                </View>
                <View
                  style={{
                    margin: 5,
                    backgroundColor: "#f44242",
                    width: width / 3 + 30,
                    height: 2
                  }}
                />
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const shop = {
  hlist: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0
  },
  title: {
    color: "#000",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "600",
    paddingTop: 14,
    paddingRight: 80,
    paddingBottom: 10,
    paddingLeft: width / 3
  },
  scrollView: {
    height: 220,
    borderBottomColor: "#eee",
    borderBottomWidth: 1
  },
  panel: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    marginLeft: 5,
    position: "relative",
    width: width / 3 - 10,
    height: width / 3
  },
  imagePanel: {
    position: "absolute",
    width: width / 3 - 10,
    height: width / 3,
    top: 0
  },
  name: {
    textAlign: "center",
    fontSize: 13,
    width: width / 3 - 15,
    paddingLeft: 6,
    paddingTop: 6
  },
  price: {
    color: "#999",
    fontSize: 12,
    textAlign: "center",
    paddingTop: 6
  }
};
