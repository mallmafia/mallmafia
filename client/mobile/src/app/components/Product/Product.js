import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import Header from "@commons/components/Header";
import ProductSwiper from "./ProductSwiper";
import ProductDetails from "./ProductDetails";
import BottomButtons from "./BottomButtons";

// import { Fetch } from "@data/actions";
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// import { DropDownMsg } from "../EventView";

class Product extends Component {
  componentDidMount() {}
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="Product" searchButton cartButton static />
        <ScrollView>
          <ProductSwiper images={this.props.product.imgs} />
          <ProductDetails details={this.props.product} />
        </ScrollView>
        <BottomButtons product={this.props.product._id} />
      </View>
    );
  }
}

export default Product;
