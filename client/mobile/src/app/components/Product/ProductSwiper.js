import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  ScrollView,
  Dimensions
} from "react-native";
import Swiper from "react-native-swiper";

const { width, height, scale } = Dimensions.get("window");

export default class ProductSwiper extends Component {
  render() {
    if (this.props.images.length > 0) {
      return (
        <View
          // source={require("@images/fossil.jpg")}
          style={{
            width,
            height: 300
          }}
        >
          <Swiper
            dot={
              <View
                style={{
                  backgroundColor: "#F00",
                  width: 8,
                  height: 8,
                  borderRadius: 4,
                  marginLeft: 4,
                  marginRight: 4
                }}
              />
            }
            activeDot={
              <View
                style={{
                  backgroundColor: "#fff",
                  width: 8,
                  height: 8,
                  borderRadius: 4,
                  marginLeft: 4,
                  marginRight: 4
                }}
              />
            }
            // paginationStyle={{ top: -300, left: 300 }}
            autoplay
            autoplayDirection
            autoplayTimeout={3.0}
            style={{ height: 300 }}
          >
            {this.props.images.map(img =>
              <View>
                <Image
                  source={{ uri: img }}
                  style={{
                    width,
                    height: 300,
                    resizeMode: "contain"
                  }}
                />
              </View>
            )}
          </Swiper>
        </View>
      );
    }
    return (
      <Image
        source={require("@images/fossil.jpg")}
        style={{
          width,
          height: 300
        }}
      >
        <Swiper
          dot={
            <View
              style={{
                backgroundColor: "#F00",
                width: 8,
                height: 8,
                borderRadius: 4,
                marginLeft: 4,
                marginRight: 4
              }}
            />
          }
          activeDot={
            <View
              style={{
                backgroundColor: "#fff",
                width: 8,
                height: 8,
                borderRadius: 4,
                marginLeft: 4,
                marginRight: 4
              }}
            />
          }
          // paginationStyle={{ top: -300, left: 300 }}
          autoplay
          autoplayDirection
          autoplayTimeout={3.0}
          style={{ height: 300 }}
        >
          <View>
            <Image
              source={require("@images/fossil2.jpg")}
              style={{
                width,
                height: 300
              }}
            />
          </View>
          <View>
            <Image
              source={require("@images/fossil3.jpg")}
              style={{
                width,
                height: 300
              }}
            />
          </View>
          <View>
            <Image
              source={require("@images/fossil4.jpg")}
              style={{
                width,
                height: 300
              }}
            />
          </View>
        </Swiper>
      </Image>
    );
  }
}
