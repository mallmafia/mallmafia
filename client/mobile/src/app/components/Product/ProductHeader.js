import React, { Component } from "react";
import { View, ScrollView, Text, Dimensions } from "react-native";

export default class ProductHeader extends Component {
  render() {
    return (
      <View style={{ flexDirection: "row" }}>
        <View
          style={{
            backgroundColor: "#f44242",
            alignSelf: "flex-start",
            padding: 10,
            borderRightColor: "#fff",
            borderRightWidth: 3,
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10
          }}
        >
          <Text
            style={{
              color: "#fff",
              fontWeight: "500"
            }}
          >
            {this.props.name}
          </Text>
        </View>
      </View>
    );
  }
}
