import React, { Component } from "react";
import { View, ScrollView, Text, Dimensions } from "react-native";

export default class ProductDescription extends Component {
  render() {
    return (
      <View>
        <Text
          style={{
            color: "#000",
            fontSize: 17,
            fontWeight: "400",
            marginLeft: 10,
            marginTop: 10,
            marginRight: 10
          }}
        >
          Product Description
        </Text>
        <Text style={{ color: "#000" }}>{this.props.description}</Text>
      </View>
    );
  }
}
