import React, { Component } from "react";
import { View, ScrollView, Text, Dimensions } from "react-native";

const { width } = Dimensions.get("window");
export default class ProductInformationTable extends Component {
  render() {
    return (
      <View style={{ marginTop: 10 }}>
        <Text
          style={{ color: "#000", fontSize: 17, fontWeight: "400", margin: 10 }}
        >
          Product Information
        </Text>
        <RowInfo head="Department" headValue="Men" />
        <RowInfo head="Manufacturer" headValue="Fossil" />
        <RowInfo head="Item part number" headValue="FS4552" />
        <RowInfo
          head="Product Dimensions"
          headValue="4.3 x 4.3 x 1.3 cm; 168 g"
        />
        <RowInfo head="Band Colour" headValue="Black" />
      </View>
    );
  }
}

const RowInfo = props =>
  <View
    style={{
      flexDirection: "row"
    }}
  >
    <View
      style={{
        backgroundColor: "#f3f3f3",
        borderWidth: 1,
        borderColor: "#e1e1e1",
        width: width / 2 - 10
      }}
    >
      <Text style={{ color: "#000", margin: 10 }}>
        {props.head}
      </Text>
    </View>

    <View
      style={{
        width: width / 2 - 10,
        borderWidth: 1,
        borderColor: "#e1e1e1"
      }}
    >
      <Text style={{ color: "#000", margin: 10 }}>
        {props.headValue}
      </Text>
    </View>
  </View>;
