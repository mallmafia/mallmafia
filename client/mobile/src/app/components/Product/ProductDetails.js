import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import ProductHeader from "./ProductHeader";
import StarRating from "@commons/components/StarRating";
import ProductInformationTable from "./ProductInformationTable";
import ProductDescription from "./ProductDescription";

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5
    };
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
  render() {
    return (
      <View style={{ margin: 10 }}>
        <ProductHeader name={this.props.details.name} />
        <View
          style={{
            marginTop: 20,
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Text
            style={{
              color: "#f00",
              fontWeight: "700",
              fontSize: 25
            }}
          >
            {" "}
            &#8377; {this.props.details.price}
          </Text>
          <StarRating
            disabled={false}
            maxStars={5}
            rating={this.props.details.rating}
            selectedStar={rating => this.onStarRatingPress(rating)}
            starColor={"#f44242"}
          />
        </View>
        <View>
          <Text style={{ color: "#000", fontSize: 18 }}>
            Sold by{" "}
            <Text style={{ color: "#f00", fontWeight: "600" }}>
              {this.props.details.store_name}
            </Text>
          </Text>
        </View>
        <ProductDescription description={this.props.details.description} />
        <ProductInformationTable />
      </View>
    );
  }
}
