import React, { Component } from "react";
import { Dimensions, View, Text, TouchableOpacity } from "react-native";
import { ON_ALERT, OFF_ALERT } from "@data/constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Fetch } from "@data/actions";
import { Actions } from "react-native-router-flux";
const { width } = Dimensions.get("window");
class BottomButtons extends Component {
  render() {
    const redirectToLogin = () => {
      alert("Please login to add products to cart");
      Actions.auth();
    };
    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <TouchableOpacity
          onPress={
            this.props.entities.user ? (
              () => this.props.addToCart()
            ) : (
              () => redirectToLogin()
            )
          }
        >
          <View
            style={{
              backgroundColor: "rgb(244, 66, 66)",
              width: width / 2,
              height: 40,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "#fff", alignItems: "center" }}>
              {" "}
              Add to Cart
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity>
          <View
            style={{
              backgroundColor: "#000",
              width: width / 2,
              height: 40,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "#fff", alignItems: "center" }}>
              {" "}
              Buy Now
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  let product = ownProps.product;
  return {
    addToCart: product =>
      dispatch({
        type: ON_ALERT,
        payload: {
          text: "Do you want to add this item to cart?",
          buttons: [
            {
              label: "YES",
              onPress: () => {
                dispatch({ type: OFF_ALERT });
                dispatch(
                  Fetch(
                    {
                      method: "post",
                      route: `user/cart/`,
                      name: "cart",
                      params: { productId: ownProps.product }
                    },
                    () =>
                      dispatch({
                        type: ON_ALERT,
                        payload: {
                          text: "Item added to cart",
                          buttons: [
                            {
                              label: "OK",
                              onPress: () => {
                                dispatch({ type: OFF_ALERT });
                                dispatch(
                                  Fetch(
                                    {
                                      method: "get",
                                      route: `user`,
                                      name: "user"
                                    },
                                    () => null
                                  )
                                );
                              }
                            }
                          ]
                        }
                      })
                  )
                );
              }
            },
            { label: "NO", onPress: () => dispatch({ type: OFF_ALERT }) }
          ]
        }
      })
  };
};
const mapStateToProps = state => {
  return state.FetchState;
};
export default connect(mapStateToProps, mapDispatchToProps)(BottomButtons);
