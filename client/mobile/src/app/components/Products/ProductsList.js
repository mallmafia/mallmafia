import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image
} from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
import StarRating from "@commons/components/StarRating";
import { connect } from "react-redux";
import { Fetch } from "@data/actions";
import { AsyncGet } from "@utils";
import { bindActionCreators } from "redux";
import Header from "@commons/components/Header";
class ProductList extends Component {
  componentDidMount() {
    AsyncGet("@mall", mall => {
      this.setState({ index: mall.index });

      this.props.Fetch(
        {
          method: "get",
          route: `mall/${mall.key + 1}/products`,
          name: "mall_products"
        },
        () => null
      );
      return;
    });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="Products" searchButton cartButton static />
        <ScrollView>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap"
            }}
          >
            {this.props.entities.mall_products ? (
              this.props.entities.mall_products.map(product => (
                <TouchableOpacity
                  style={{ margin: 5, marginBottom: 10 }}
                  onPress={() => Actions.product({ product: product })}
                >
                  <View style={{ alignItems: "center" }}>
                    <Image
                      source={{ uri: product.showcase }}
                      style={{
                        width: width / 3 + 50,
                        height: height / 3,
                        resizeMode: "contain"
                      }}
                    />
                    <Text style={{ fontSize: 16 }}>
                      {product.name.slice(0, 15)}{" "}
                    </Text>
                    <Text style={{ fontFamily: "sans-serif-condensed" }}>
                      &#8377; {product.price}
                    </Text>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      rating={product.rating}
                      starColor={"#f44242"}
                      starSize={18}
                    />
                  </View>
                  <View
                    style={{
                      margin: 5,
                      backgroundColor: "#f44242",
                      width: width / 3 + 30,
                      height: 2
                    }}
                  />
                </TouchableOpacity>
              ))
            ) : (
              <View>
                <Text>Loading....</Text>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const shop = {
  hlist: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0
  },
  title: {
    color: "#000",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "600",
    paddingTop: 14,
    paddingRight: 80,
    paddingBottom: 10,
    paddingLeft: width / 3
  },
  scrollView: {
    height: 220,
    borderBottomColor: "#eee",
    borderBottomWidth: 1
  },
  panel: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    marginLeft: 5,
    position: "relative",
    width: width / 3 - 10,
    height: width / 3
  },
  imagePanel: {
    position: "absolute",
    width: width / 3 - 10,
    height: width / 3,
    top: 0
  },
  name: {
    textAlign: "center",
    fontSize: 13,
    width: width / 3 - 15,
    paddingLeft: 6,
    paddingTop: 6
  },
  price: {
    color: "#999",
    fontSize: 12,
    textAlign: "center",
    paddingTop: 6
  }
};
const mapStateToProps = state => {
  return state.FetchState;
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return bindActionCreators({ Fetch }, dispatch);
  // dispatch(Fetch({name: "allproducts",method: "get",route: "products?ids="}))
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
