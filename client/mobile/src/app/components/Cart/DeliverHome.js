import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  Image,
  ScrollView,
  BackAndroid,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Dimensions
} from "react-native";
import store from "@data/store";
import { ON_ALERT, OFF_ALERT } from "@data/constants";
import Icon from "react-native-vector-icons/EvilIcons";
import { connect } from "react-redux";
import { Fetch } from "@data/actions";
import Header from "@commons/components/Header";
import { Spinner, DropDownMsg, Alert } from "../EventView";
import { AsyncGet } from "@utils/AsyncGet";
import CartItem from "./CartItem";
import { Actions } from "react-native-router-flux";
const { width, height } = Dimensions.get("window");

class DeliverHome extends Component {
  render() {
    return (
      <View style={{}}>
        <Header searchButton cartButton backButton secondaryname="Delivery" />
        <View
          style={{
            alignItems: "center",
            backgroundColor: "#f44242"
          }}
        >
          <View
            style={{
              width: width - 10,
              height: height - 10,
              backgroundColor: "#fff"
            }}
          >
            <View style={{ borderColor: "#f1f1f1", borderWidth: 8 }}>
              <View style={{ margin: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "600" }}>Fahid M</Text>
                <Text style={{ fontSize: 13 }}>
                  KIT College, KIT Food Court
                </Text>
                <Text style={{ fontSize: 13 }}>Coimbatore, Tamilnadu</Text>
                <Text style={{ fontSize: 13 }}>641001</Text>
                <TouchableOpacity style={{ marginTop: 10 }}>
                  <View
                    style={{
                      backgroundColor: "#f44242",
                      width: width - 50,
                      borderRadius: 30,
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        margin: 10,
                        color: "#fff",
                        fontSize: 15,
                        fontWeight: "600"
                      }}
                    >
                      Add Address or Change the Address
                    </Text>
                    {/* {} */}
                  </View>
                </TouchableOpacity>
              </View>

              {/* {} */}
            </View>

            {this.props.entities ? this.props.entities.cart ? (
              this.props.entities.cart.map(item => (
                <TouchableOpacity>
                  <CartItem
                    notDeletable
                    cart={this.props.entities.cart}
                    item={item.id}
                    quantity={this.props.entities.user.cart.filter(
                      cart => cart.productId === item._id
                    )}
                    maxQuantity={item.quantity}
                    userCart={this.props.entities.user}
                    size={"Size 9"}
                    price={`${item.price}`}
                    image={{ uri: item.showcase }}
                    name={item.name}
                  />
                </TouchableOpacity>
              ))
            ) : null : null}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};
export default connect(mapStateToprops, mapDispatchToProps)(DeliverHome);
