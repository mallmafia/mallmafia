"use strict";

import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Image,
  StatusBarIOS,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  DeviceEventEmitter,
  Picker
} from "react-native";

import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/EvilIcons";
const { width, height } = Dimensions.get("window");
import { connect } from "react-redux";
import { Fetch } from "@data/actions";
import { ON_LOAD, OFF_LOAD } from "@data/constants";
const cart = {
  buttonPanel: {
    alignItems: "flex-end",
    justifyContent: "center",
    paddingBottom: 6,
    width: 46
  },
  buttons: {
    borderWidth: 0.8,
    borderColor: "#ddd",
    borderRadius: 4,
    overflow: "hidden",
    width: 38
  },
  cards: {
    position: "relative",
    backgroundColor: "#fff",
    width: width - 20,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 36,
    marginLeft: 10,
    marginRight: 10,
    borderTopColor: "#dddddd",
    borderTopWidth: 1
  },
  numberItem: {
    fontSize: 15,
    color: "#666",
    fontWeight: "300",
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
    width: 34,
    height: 26,
    paddingTop: 4,
    textAlign: "center"
  },
  numberText: {
    color: "#333",
    fontWeight: "500",
    backgroundColor: "white",
    paddingTop: 6,
    paddingBottom: 6
  },
  namePanel: {
    width: 3 * width / 4 - 104,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10
  },
  close: {
    left: -2,
    top: 60,
    position: "absolute"
  },
  active: {
    color: "#1CAADE"
  },
  iconClose: {
    color: "#999",
    fontSize: 30
  },
  image: {
    resizeMode: "contain",
    width: width / 4,
    height: 120
  }
};
const css = {};
class CartItem extends Component {
  state = {
    language: "english"
  };
  render() {
    const deleteCartItem = item => {
      this.props.removeCart(item);
    };
    const makeArray = count => {
      var results = [];
      for (let i = 1; i <= count; i++) {
        results.push({ label: String(i), value: i });
      }
      return results;
    };
    const buttonList = (
      <View style={cart.buttonPanel}>
        <View style={cart.buttons}>
          <View
            style={{
              borderWidth: 0.8,
              borderColor: "#ddd",
              borderRadius: 4,
              overflow: "hidden",
              width: 70
            }}
          >
            <Picker
              style={{ width: 80, height: 44 }}
              itemStyle={{ height: 44 }}
              prompt={"Pick the quantity"}
              selectedValue={this.props.quantity[0].quantity}
              onValueChange={(itemValue, itemIndex) =>
                this.props.updateCart(this.props.item, itemValue)}
            >
              {makeArray(this.props.maxQuantity).map(picketitem => (
                <Picker.Item
                  label={picketitem.label}
                  value={picketitem.value}
                />
              ))}
            </Picker>
          </View>
        </View>
      </View>
    );

    return (
      <View style={cart.cards}>
        {this.props.notDeletable ? null : (
          <TouchableOpacity
            style={cart.close}
            onPress={() => deleteCartItem(this.props.item)}
          >
            <Icon
              name={"trash"}
              style={[
                css.icon32,
                cart.iconClose,
                this.props.isSelect == true ? cart.active : ""
              ]}
            />
          </TouchableOpacity>
        )}
        <View style={{ flexDirection: "row" }}>
          <Image style={cart.image} source={this.props.image} />

          <View style={cart.namePanel}>
            <Text style={cart.name}>{this.props.name}</Text>
            <View>
              <Text style={cart.price}>&#8377;{this.props.price}</Text>
              <Text style={cart.size}>{this.props.size}</Text>
            </View>
          </View>
          {this.props.hideButton ? null : buttonList}
        </View>
      </View>
    );
  }
}
const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const cart = ownProps.cart;
  let user = ownProps.userCart;
  return {
    removeCart: item => {
      dispatch(
        Fetch(
          {
            method: "put",
            route: `user/cart/`,
            name: "cart",
            params: {
              productId: item
            }
          },
          () =>
            dispatch(
              Fetch(
                {
                  method: "get",
                  route: `user/`,
                  name: "user"
                },
                () => null
              )
            )
        )
      );
    },
    updateCart: (productId, quantity) =>
      dispatch(
        Fetch(
          {
            method: "put",
            route: `user/cart/`,
            name: "cart",
            params: {
              productId: productId,
              quantity: quantity
            }
          },
          () =>
            dispatch(
              Fetch(
                {
                  method: "get",
                  route: `user/`,
                  name: "user"
                },
                () => null
              )
            )
        )
      )
  };
};
export default connect(mapStateToprops, mapDispatchToProps)(CartItem);
