import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  Image,
  ScrollView,
  BackAndroid,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Dimensions
} from "react-native";
import store from "@data/store";
import { ON_ALERT, OFF_ALERT } from "@data/constants";
import Icon from "react-native-vector-icons/EvilIcons";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Fetch } from "@data/actions";
import Header from "@commons/components/Header";
import { Spinner, DropDownMsg, Alert } from "../EventView";
import { AsyncGet } from "@utils/AsyncGet";
import CartItem from "./CartItem";
import { Actions } from "react-native-router-flux";
const { width, height } = Dimensions.get("window");
import makeTransaction from "./makeTransaction";
import RazorpayCheckout from "react-native-razorpay";
const cart = {
  total: {
    flexDirection: "row",
    paddingTop: 10,
    paddingRight: 20,
    paddingBottom: 10,
    paddingLeft: 20,
    borderTopWidth: 1,
    borderTopColor: "#ddd",
    width: width
  },
  totalText: {
    color: "#999",
    fontSize: 12,
    paddingTop: 6,
    justifyContent: "center",
    width: width / 2 - 10
  },
  totalPrice: {
    color: "#333",
    alignItems: "center",
    textAlign: "right",
    paddingRight: 30,
    fontWeight: "600",
    width: width / 2
  },
  totalPriceAndroid: {
    color: "#333",
    alignItems: "center",
    textAlign: "right",
    paddingRight: 60,
    fontWeight: "600",
    width: width / 2 - 30
  }
};
class Cart extends Component {
  componentDidMount() {
    this.props.fetchCart();
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.props.entities != nextProps.entities) {
      return true;
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="Cart" searchButton cartButton static />
        <ScrollView>
          {this.props.entities ? this.props.entities.cart ? (
            this.props.entities.cart.map(item => (
              <TouchableOpacity
                onPress={() => Actions.product({ product: item })}
              >
                <CartItem
                  cart={this.props.entities.cart}
                  item={item._id}
                  quantity={this.props.entities.user.cart.filter(
                    cart => cart.productId === item._id
                  )}
                  maxQuantity={item.quantity}
                  userCart={this.props.entities.user}
                  size={"Size 9"}
                  price={`${item.price}`}
                  image={{ uri: item.showcase }}
                  name={item.name}
                />
              </TouchableOpacity>
            ))
          ) : null : null}
        </ScrollView>
        <View style={cart.total}>
          <Text style={cart.totalText}>TOTAL</Text>
          <Text
            style={
              Platform.OS === "ios" ? cart.totalPrice : cart.totalPriceAndroid
            }
          >
            {this.props.entities.user ? (
              this.props.entities.user.cart_total_price
            ) : (
              0
            )}
          </Text>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity onPress={this.props.confirmTakeAway}>
            <View
              style={{
                backgroundColor: "rgb(244, 66, 66)",
                width: width / 2,
                height: 40,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{ color: "#fff", alignItems: "center" }}>
                {" "}
                Take Away
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={Actions.deliverhome}>
            <View
              style={{
                backgroundColor: "#000",
                width: width / 2,
                height: 40,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{ color: "#fff", alignItems: "center" }}>
                Deliver Home
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCart: () =>
      dispatch(
        Fetch(
          {
            method: "get",
            route: `user/cart/`,
            name: "cart"
          },
          () => null
        )
      ),
    confirmTakeAway: () =>
      dispatch(
        {
          type: ON_ALERT,
          payload: {
            text: "Do you want to place this order?",
            buttons: [
              {
                label: "Yes",
                onPress: () => {
                  let data = store.getState().FetchState.entities;
                  makeTransaction(
                    data.user.cart_total_price,
                    data.user.email,
                    data.user.phone,
                    data.user.fullName
                  );
                  console.log(store.getState().FetchState.entities);
                }
              },
              {
                label: "No",
                onPress: () => {
                  dispatch({ type: OFF_ALERT });
                  dispatch(
                    Fetch(
                      {
                        method: "get",
                        route: `user`,
                        name: "user"
                      },
                      () => null
                    )
                  );
                }
              }
            ]
          }
        },
        () => dispatch({ type: OFF_ALERT }, () => null)
      )
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(Cart);
