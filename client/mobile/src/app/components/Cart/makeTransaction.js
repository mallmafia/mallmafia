import RazorpayCheckout from "react-native-razorpay";
import { Fetch } from "@data/actions";
import store from "@data/store";
import { ON_ALERT, OFF_ALERT } from "@data/constants";
export default function(amount, email, phone, name) {
  var options = {
    description: "Pay MartBee for this product",
    image: "https://i.imgur.com/3g7nmJC.png",
    currency: "INR",
    key: "rzp_test_aUCZhy0FtHhmLo",
    amount: amount,
    name: "MartBee",
    prefill: {
      email: email,
      contact: phone,
      name: name
    },
    theme: { color: "#f44242" }
  };
  RazorpayCheckout.open(options)
    .then(data => {
      // handle success
      // alert(`Success: ${data.razorpay_payment_id}`);
      alert("Successful payment");
      console.log("workin");
      store.dispatch({ type: OFF_ALERT });
      store.dispatch(
        Fetch(
          {
            method: "post",
            route: `user/order`,
            name: "cart",
            params: [{ order_type: 2 }]
          },
          () =>
            store.dispatch(
              Fetch(
                {
                  method: "get",
                  route: `user`,
                  name: "user"
                },
                () => null
              )
            )
        )
      );
    })
    .catch(error => {
      store.dispatch({ type: OFF_ALERT });
      alert("Payment failed/cancelled");
      // handle failure
      console.log(error);
    });
}
