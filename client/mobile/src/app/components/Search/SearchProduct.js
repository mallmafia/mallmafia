import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList
} from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
import IconImage from "./IconImage";
import Icon from "react-native-vector-icons/EvilIcons";
import { connect } from "react-redux";
import { Request, Flush } from "@data/actions";
import Header from "@commons/components/Header";
import StarRating from "@commons/components/StarRating";
class SearchProduct extends Component {
  state = {
    search_term: ""
  };

  componentWillUnmount() {
    if (!this.state.search_term) {
      this.props.flush("search_results");
    }
  }
  render() {
    return (
      <View>
        <View
          style={{
            backgroundColor: "#f44242",
            height: 70,
            width: width,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 7,
              height: 40,
              backgroundColor: "#ed6f6f"
            }}
          >
            <IconImage image={require("@images/icon-search.png")} />
            <TextInput
              placeholder="Search for products"
              placeholderTextColor="rgba(255,255,255,0.6)"
              onChangeText={text => {
                this.setState({ search_term: text });
                console.log(text);
              }}
              style={{
                backgroundColor: "#ed6f6f",
                width: width / 1.7,
                height: 40,
                borderRadius: 8,
                color: "white"
              }}
              underlineColorAndroid="rgba(0,0,0,0)"
              selectionColor="white"
              value={this.state.search_term}
            />
            <TouchableOpacity
              onPress={() => this.setState({ search_term: "" })}
            >
              <Icon
                name={"close"}
                style={{
                  fontSize: 25,
                  color: "white"
                }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.setState({ search_result_term: this.state.search_term });
              this.props.search(this.state.search_term);
            }}
          >
            <Text style={{ color: "white", marginLeft: 10 }}>Search</Text>
          </TouchableOpacity>
        </View>
        {this.props.search_results ? (
          <View>
            <View style={{ marginTop: 5, marginLeft: 5 }}>
              <Text>
                {this.props.search_results.length ? (
                  `${this.props.search_results.length} results found for ${this
                    .state.search_term}`
                ) : (
                  `No results found for ${this.state.search_result_term}`
                )}
              </Text>
            </View>
            <ScrollView style={{ margin: 15 }}>
              {this.props.search_results.length ? (
                this.props.search_results.map(product => (
                  <TouchableOpacity
                    onPress={() => Actions.product({ product })}
                  >
                    <View
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#d1d1d1"
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <View style={{ justifyContent: "space-between" }}>
                          <Text>{product.name.slice(0, 35)}</Text>
                          <Text style={{ color: "#000" }}>
                            &#8377;{product.price}
                          </Text>

                          <View style={{ width: 60 }}>
                            <StarRating
                              disabled={false}
                              maxStars={5}
                              rating={product.rating}
                              starColor={"#f44242"}
                              starSize={18}
                            />
                          </View>
                          <Text>by {product.store_name}</Text>
                        </View>
                        <Image
                          source={{ uri: product.showcase }}
                          style={{
                            width: 80,
                            height: 75,
                            resizeMode: "contain"
                          }}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                ))
              ) : null}
            </ScrollView>
          </View>
        ) : (
          <View>
            <Text>There is no results here. Try searching</Text>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state.Entities;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    search: search_term =>
      dispatch(
        Request(
          {
            name: "search_results",
            method: "GET",
            route: `search?q=${search_term}`
          },
          () => null
        )
      ),
    flush: name => dispatch(Flush({ name: name }))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SearchProduct);
