import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList
} from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
import IconImage from "./IconImage";
import Icon from "react-native-vector-icons/EvilIcons";
import Header from "@commons/components/Header";
export default class SearchInStore extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            backgroundColor: "#f44242",
            height: 70,
            width: width,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 7,
              height: 40,
              backgroundColor: "#ed6f6f"
            }}
          >
            <IconImage image={require("@images/icon-search.png")} />
            <TextInput
              style={{
                backgroundColor: "#ed6f6f",
                width: width / 1.7,
                height: 40,
                borderRadius: 8,
                color: "white"
              }}
              underlineColorAndroid="rgba(0,0,0,0)"
              selectionColor="white"
              placeholderTextColor={"#aaa"}
            />
            <Icon
              name={"close"}
              style={{
                fontSize: 25,
                color: "white"
              }}
            />
          </View>
          <Text style={{ color: "white", marginLeft: 10 }}>Cancel</Text>
        </View>
        <Image
          source={require("./4.jpg")}
          style={{
            height: 400,
            width: width
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              width: width,
              height: height / 5,
              marginTop: height / 1.5
            }}
          >
            <View
              style={{
                flexDirection: "row",
                borderBottomWidth: 0.7,
                borderColor: "grey"
              }}
            >
              <Image
                source={{
                  uri:
                    "https://cdn.shopify.com/s/files/1/0174/1800/products/LiPo_1_of_8_f804ae92-bf3e-4439-aa0d-285f12014623_1024x1024.JPG?v=1466671803"
                }}
                style={{ height: 65, width: 75, marginTop: 20, marginLeft: 15 }}
              />
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    marginTop: 20,
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "black",
                    marginBottom: 5,
                    marginLeft: 20
                  }}
                >
                  11- 20V Lithium Battery
                </Text>
                <Text
                  style={{ marginLeft: 25, color: "#f44242", marginBottom: 5 }}
                >
                  Rack No: 11
                </Text>
              </View>
            </View>
          </View>
        </Image>
      </View>
    );
  }
}
