import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList
} from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
import IconImage from "./IconImage";
import Icon from "react-native-vector-icons/EvilIcons";
import Header from "@commons/components/Header";
export default class SearchMap extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            backgroundColor: "#f44242",
            height: 70,
            width: width,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 7,
              height: 40,
              backgroundColor: "#ed6f6f"
            }}
          >
            <IconImage image={require("@images/icon-search.png")} />
            <TextInput
              style={{
                backgroundColor: "#ed6f6f",
                width: width / 1.7,
                height: 40,
                borderRadius: 8,
                color: "white"
              }}
              underlineColorAndroid="rgba(0,0,0,0)"
              selectionColor="white"
              placeholderTextColor={"#aaa"}
            />
            <Icon
              name={"close"}
              style={{
                fontSize: 25,
                color: "white"
              }}
            />
          </View>
          <Text style={{ color: "white", marginLeft: 10 }}>Cancel</Text>
        </View>
        <Image
          source={require("./map.jpg")}
          style={{
            height: height,
            width: width
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              width: width,
              height: height / 3,
              marginTop: height / 2
            }}
          >
            <View
              style={{
                flexDirection: "row",
                borderBottomWidth: 0.7,
                borderColor: "grey"
              }}
            >
              <Image
                source={require("./tool.png")}
                style={{ height: 65, width: 75, marginTop: 20, marginLeft: 15 }}
              />
              <TouchableOpacity onPress={Actions.searchinstore}>
                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={{
                      marginTop: 20,
                      fontWeight: "bold",
                      fontSize: 20,
                      color: "black",
                      marginBottom: 5
                    }}
                  >
                    JD Electricals and Hardwares
                  </Text>
                  <Text
                    style={{ marginLeft: 8, color: "#f44242", marginBottom: 5 }}
                  >
                    Light Bulb
                  </Text>
                  <Text style={{ marginLeft: 8, marginBottom: 5 }}>
                    mi 0.8-Hopes,TP Complex
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: "row",
                borderBottomWidth: 0.7,
                borderColor: "grey"
              }}
            >
              <Image
                source={require("./ace.png")}
                style={{ height: 65, width: 75, marginTop: 20, marginLeft: 15 }}
              />
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    marginTop: 20,
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "black",
                    marginBottom: 5
                  }}
                >
                  ACE Electricals and Tools
                </Text>
                <Text
                  style={{ marginLeft: 8, color: "#f44242", marginBottom: 5 }}
                >
                  Light Bulb
                </Text>
                <Text style={{ marginLeft: 8, marginBottom: 5 }}>
                  mi 0.4-Singanallur,UP Complex
                </Text>
              </View>
            </View>
          </View>
        </Image>
      </View>
    );
  }
}
