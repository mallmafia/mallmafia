import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  Image,
  ScrollView,
  BackAndroid,
  TouchableOpacity,
  TextInput,
  ActivityIndicator
} from "react-native";
import ScrollableTabView, {
  ScrollableTabBar
} from "react-native-scrollable-tab-view";
import Icon from "react-native-vector-icons/EvilIcons";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Fetch } from "@data/actions";

import HorizontalItem from "./Components/HorizontalItem";
import AppEventEmitter from "@utils/AppEventEmitter";
import home from "@styles/home";
import Header from "@commons/components/Header";
import { Spinner, DropDownMsg, Alert } from "../EventView";
import { AsyncGet } from "@utils/AsyncGet";
class MarketScreen extends Component {
  componentDidMount() {
    AsyncGet("@mall", mall => {
      this.setState({ index: mall.index });
      this.props.Fetch(
        { method: "get", route: `mall/${mall.key + 1}/`, name: "selectedMall" },
        () => null
      );
      return;
    });
  }
  open() {
    AppEventEmitter.emit("hamburger.click");
  }
  render() {
    const { entities } = this.props;

    return (
      <View style={home.color}>
        <Spinner />
        <Header name="MARKET" searchButton cartButton mallSelect />
        <View style={[home.search]}>
          <TextInput
            underlineColorAndroid="rgba(0,0,0,0)"
            style={home.searchBar}
            placeholder={"Looking for a gift.."}
            selectionColor={"#7d59c8"}
          />
          <Icon name={"search"} style={home.searchIcon} />
        </View>

        <View style={home.menu}>
          <HorizontalItem mallData={entities.selectedMall} />
        </View>
        <Alert />
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Fetch: Fetch }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(MarketScreen);
