import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  Image,
  ScrollView,
  BackAndroid,
  TouchableOpacity,
  TextInput,
  Dimensions
} from "react-native";
import Icon from "react-native-vector-icons/EvilIcons";
import AppEventEmitter from "@utils/AppEventEmitter";
import home from "@styles/home";
import Header from "@commons/Header";

const { width, height, scale } = Dimensions.get("window");

export default class CategoryList extends Component {
  render() {
    return (
      <View style={home.color}>
        <Header name="MARKET" searchButton cartButton />
        <View
          style={{
            backgroundColor: "grey",
            flexDirection: "row",
            width,
            height: 45,
            justifyContent: "center"
          }}
        >
          <View
            style={{
              width: width / 1.2,
              height: 35,
              backgroundColor: "white",
              marginLeft: 15,
              borderRadius: 15,
              flexDirection: "row",
              marginTop: 5
            }}
          >
            <TextInput
              style={{
                width: width / 1.4,
                fontSize: 16,
                backgroundColor: "white",
                marginLeft: 10
              }}
              placeholder="search by Category"
              placeholderTextColor="grey"
              underlineColorAndroid="white"
            />
            <Image
              style={{ height: 20, width: 20, marginTop: 3 }}
              source={require("./Images/icon-search.png")}
            />
          </View>
        </View>
      </View>
    );
  }
}
