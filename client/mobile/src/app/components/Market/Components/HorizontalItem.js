import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from "react-native";
import { Actions } from "react-native-router-flux";
import Swiper from "react-native-swiper";

const shop = require("./../Styles/shop.js").default;

import Product from "./Product";
import Category from "./Category";

export default class HorizontalItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const swipeView = (
      <View>
        <Image source={require("./../Images/mall.jpg")} style={shop.image}>
          {this.props.mallData ? (
            <Swiper
              dot={
                <View
                  style={{
                    backgroundColor: "rgba(255,255,255,.3)",
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4
                  }}
                />
              }
              activeDot={
                <View
                  style={{
                    backgroundColor: "#fff",
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4
                  }}
                />
              }
              paginationStyle={{ top: -300, left: 300 }}
            >
              {this.props.mallData.mall.banner_imgs.map(img => (
                <Image source={{ uri: img }} style={shop.image} />
              ))}
            </Swiper>
          ) : (
            <Swiper
              dot={
                <View
                  style={{
                    backgroundColor: "rgba(255,255,255,.3)",
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4
                  }}
                />
              }
              activeDot={
                <View
                  style={{
                    backgroundColor: "#fff",
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                    marginLeft: 4,
                    marginRight: 4
                  }}
                />
              }
              paginationStyle={{ top: -300, left: 300 }}
            >
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
              <ActivityIndicator
                size={"large"}
                style={[shop.image, { height: 350 }]}
              />
            </Swiper>
          )}
        </Image>
      </View>
    );

    const scrollView1 = (
      <ScrollView
        style={shop.scrollView}
        directionalLockEnabled
        horizontal
        pagingEnabled
      >
        <View style={{ flexDirection: "row" }}>
          {this.props.mallData ? this.props.mallData.top_products ? (
            this.props.mallData.top_products.map(product => (
              <TouchableOpacity onPress={() => Actions.product({ product })}>
                <Product
                  name={product.name}
                  price={product.price}
                  imageURL={{ uri: product.showcase }}
                />
              </TouchableOpacity>
            ))
          ) : null : null}

          <Product
            name="Dark Future Drop Crotch Jeans"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Spitfire Post Punk Round"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Puma Throwbacks Sweat Shorts"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Sweat Shorts Adidas"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
        </View>
      </ScrollView>
    );

    const scrollView2 = (
      <ScrollView style={shop.scrollView} directionalLockEnabled horizontal>
        <View style={{ flexDirection: "row" }}>
          <Product
            name="what deal evil rent by real"
            price="110$"
            imageURL={require("./../Images/product1.jpg")}
          />
          <Product
            name="literature to or an sympathize"
            price="110$"
            imageURL={require("./../Images/product2.png")}
          />
          <Product
            name="Way advantage age led"
            price="110$"
            imageURL={require("./../Images/product3.jpg")}
          />
          <Product
            name="what deal evil rent by real in"
            price="110$"
            imageURL={require("./../Images/product4.jpg")}
          />
        </View>
      </ScrollView>
    );
    const categories = (
      <ScrollView style={shop.scrollView} directionalLockEnabled horizontal>
        <View style={{ flexDirection: "row" }}>
          {this.props.mallData ? this.props.mallData.category ? (
            this.props.mallData.category.map(category => (
              <Category
                name={category.name}
                imageURL={{ uri: category.icon }}
              />
            ))
          ) : null : null}
          <Product
            name="New Balance 530 Multi Trainers"
            price="120$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Dark Future Drop Crotch Jeans"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Spitfire Post Punk Round"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Puma Throwbacks Sweat Shorts"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Sweat Shorts Adidas"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
        </View>
      </ScrollView>
    );
    const scrollView3 = (
      <ScrollView style={shop.scrollView} directionalLockEnabled horizontal>
        <View style={{ flexDirection: "row" }}>
          {this.props.mallData ? this.props.mallData.stores ? (
            this.props.mallData.stores.map(store => (
              <TouchableOpacity onPress={() => Actions.store({ shop: store })}>
                <Category
                  name={store.name}
                  imageURL={{ uri: store.showcase }}
                />
              </TouchableOpacity>
            ))
          ) : null : null}
          <Product
            name="New Balance 530 Multi Trainers"
            price="120$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Dark Future Drop Crotch Jeans"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Spitfire Post Punk Round"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Puma Throwbacks Sweat Shorts"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
          <Product
            name="Sweat Shorts Adidas"
            price="110$"
            imageURL={require("./../Images/mall2.jpg")}
          />
        </View>
      </ScrollView>
    );

    return (
      <View style={shop.hlist}>
        <ScrollView>
          {swipeView}
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text style={shop.title}>PRODUCTS</Text>
            <TouchableOpacity
              onPress={() =>
                Actions.products({
                  products: this.props.mallData
                    ? this.props.mallData.products
                      ? this.props.mallData.products
                      : null
                    : null
                })}
            >
              <View
                style={{
                  borderColor: "#f44242",
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: "#f44242",
                  margin: 10
                }}
              >
                <Text
                  style={{ margin: 5, fontWeight: "normal", color: "#fff" }}
                >
                  View all
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {scrollView1}
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text style={shop.title}>CATEGORY</Text>
            <TouchableOpacity
              onPress={() =>
                Actions.categoryshops({
                  categoryshops: this.props.mallData
                    ? this.props.mallData.category
                      ? this.props.mallData.category
                      : null
                    : null
                })}
            >
              <View
                style={{
                  borderColor: "#f44242",
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: "#f44242",
                  margin: 10
                }}
              >
                <Text
                  style={{ margin: 5, fontWeight: "normal", color: "#fff" }}
                >
                  View all
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {categories}
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text style={shop.title}> STORES</Text>
            <TouchableOpacity
              onPress={() =>
                Actions.stores({
                  stores: this.props.mallData
                    ? this.props.mallData.stores
                      ? this.props.mallData.stores
                      : null
                    : null
                })}
            >
              <View
                style={{
                  borderColor: "#f44242",
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: "#f44242",
                  margin: 10
                }}
              >
                <Text
                  style={{ margin: 5, fontWeight: "normal", color: "#fff" }}
                >
                  View all
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {scrollView3}
          <View>
            <Text style={shop.title}>TODAY'S DEALS</Text>
          </View>
          {scrollView2}
        </ScrollView>
      </View>
    );
  }
}
