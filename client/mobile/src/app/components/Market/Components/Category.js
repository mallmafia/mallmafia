"use strict";

import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
const product = require("./../Styles/shop.js").default;

export default class Category extends Component {
  render() {
    return (
      <View>
        <View style={product.panel}>
          <Image source={this.props.imageURL} style={product.imagePanel} />
        </View>

        <Text style={product.name}>
          {this.props.name}
        </Text>
      </View>
    );
  }
}
