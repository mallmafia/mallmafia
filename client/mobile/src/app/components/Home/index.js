import React from "react";
import { Text, View, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import { Header } from "@commons/components";
import StickyScrollView from "./StickyScrollView";
import Content from "./Content";

class HomeScreen extends React.Component {
  render() {
    return (
      <StickyScrollView>
        <Content />
      </StickyScrollView>
    );
  }
}

export default HomeScreen;
