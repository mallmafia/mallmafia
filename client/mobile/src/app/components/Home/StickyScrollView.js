import React, { Component } from "react";
import { View, Animated, ScrollView } from "react-native";
import StickyMenu from "./StickyMenu";
import css from "./style";

let offset = 0;
const beta = 80;

export default class StickyScrollView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paddingTop: 90,
      _animatedMenu: new Animated.Value(0)
    };
  }
  /**
     * Hide animation menu
     */
  hideMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: -60
    }).start();
    this.setState({ paddingTop: 30 });
  }

  /**
     * Show animation menu
     */
  showMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: 0
    }).start();
    this.setState({ paddingTop: 90 });
  }

  /**
     * trigger when scroll
     * @param event
     */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;

    if (event.nativeEvent.contentSize.height - currentOffset < 800) {
      // fetch data here========================================================<<<<<<<<<<<<<<<<
    }

    if (currentOffset < 90) {
      this.showMenu();
      return;
    } else if (Math.abs(offset - currentOffset) <= beta) {
      return;
    }

    if (currentOffset > offset) {
      this.hideMenu();
    } else if (currentOffset <= offset) {
      this.showMenu();
    }
    offset = currentOffset;
  }

  render() {
    return (
      <View style={css.body}>
        <StickyMenu animateMenu={this.state._animatedMenu} />
        <ScrollView
          style={{ paddingTop: this.state.paddingTop }}
          onScroll={this.onScroll.bind(this)}
          scrollEventThrottle={60}
        >
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}
