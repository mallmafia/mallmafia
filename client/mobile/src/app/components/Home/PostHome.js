import React, { Component } from "react";
import { Text, View, ScrollView } from "react-native";
import style from "./style";
import PostFeature from "./PostFeature";

export default class PostHome extends Component {
  render() {
    return (
      <View style={style.hlist}>
        <PostFeature id="kone" layout={3} />

        <View>
          <Text style={style.title}>
            {"Fearture"}
          </Text>
          <Text style={style.titleSmall}>
            {"Power by Beonews"}
          </Text>
        </View>
        <PostFeature id="ktwo" layout={2} />

        <View>
          <Text style={style.title}>
            {"Editor Choice"}
          </Text>
          <Text style={style.titleSmall}>
            {"Power by Beonews"}
          </Text>
        </View>
        <PostFeature id="kthree" layout={1} />

        <View>
          <Text style={style.title}>
            {"Recents Post"}
          </Text>
          <Text style={style.titleSmall}>
            {"Power by Beonews"}
          </Text>
        </View>

        <PostFeature id="klist" vertical layout={7} />
      </View>
    );
  }
}
