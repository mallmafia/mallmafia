"use strict";

import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import Swiper from "react-native-swiper";
import TimeAgo from "@custom/react-native-timeago";
import news from "./style";
import Tools from "@utils/tools";

import data from "@mockdata/posts";

export default class Banner extends Component {
  constructor(props) {
    super(props);
    this.data = [];
    this.state = {
      stickyPosts: null
    };
  }

  componentDidMount() {
    this.fetchPostBanner();
  }

  fetchPostBanner() {
    var self = this;
    this.setState({ stickyPosts: data });
  }

  render() {
    if (this.state.stickyPosts === null) {
      return (
        <View style={{ height: 200 }}>
          <Image
            style={news.bannerImage}
            source={require("@images/placeholderImage.png")}
          />
        </View>
      );
    }

    return (
      <View style={news.banner}>
        <Swiper
          dot={<View style={news.dot} />}
          activeDot={<View style={news.dotActive} />}
          paginationStyle={{ top: -220, right: 10 }}
        >
          {this.state.stickyPosts.map((post, i) => {
            const imageUrl = Tools.getImage(post);
            const postTitle =
              typeof post.title.rendered != "undefined"
                ? Tools.getDescription(post.title.rendered, 300)
                : "";

            return (
              <View style={news.bannerView} key={"sticky" + i}>
                <Image
                  style={news.bannerImage}
                  defaultSource={require("@images/placeholderImage.png")}
                  source={{ uri: imageUrl }}
                />
                <TouchableOpacity
                  // onPress={Actions.postDetails.bind(this, { post: post })}
                  style={news.bannerText}
                >
                  <Text style={news.bannerTitle}>
                    {postTitle}
                  </Text>
                  <Text style={news.bannerDate}>
                    <TimeAgo time={post.date} />
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </Swiper>
      </View>
    );
  }
}
