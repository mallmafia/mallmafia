import React, { Component } from "react";
import { ListView } from "react-native";
import Post from "@commons/components/Post/Index";
import data from "@mockdata/posts";
import categories from "@mockdata/categories";

import css from "./style";
import Constants from "@constants/Constants";

export default class PostFeature extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: null,
      datasource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
    };
  }

  componentDidMount() {
    this.setState({
      categories: categories,
      datasource: this.state.datasource.cloneWithRows(data)
    });
  }
  renderRow(layout, post) {
    return (
      <Post post={post} categories={this.state.categories} layout={layout} />
    );
  }

  render() {
    return (
      <ListView
        contentContainerStyle={this.props.vertical ? null : css.featureListView}
        key={this.props.id}
        dataSource={this.state.datasource}
        horizontal={this.props.vertical ? false : true}
        showsHorizontalScrollIndicator={this.props.vertical ? true : false}
        onEndReachedThreshold={100}
        // onEndReached={this.state.datasource}
        renderRow={this.renderRow.bind(this, this.props.layout)}
      />
    );
  }
}
