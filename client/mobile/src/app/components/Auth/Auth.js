import React, { Component } from "react";
import { Text, View, Dimensions } from "react-native";
import { Actions } from "react-native-router-flux";
import ScrollableTabView, {
  ScrollableTabBar
} from "react-native-scrollable-tab-view";

import Header from "@commons/components/Header";
import SignIn from "./SignIn";
import SignUp from "./SignUp";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      visibleHeight: Dimensions.get("window").height,
      scroll: false
    };
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: "white",
          flex: 1
        }}
      >
        <Header name="Login / Sign Up" searchButton />
        <View style={{ flex: 1 }}>
          <ScrollableTabView
            initialPage={0}
            locked={false}
            tabBarUnderlineStyle={{ height: 2, backgroundColor: "#f44242" }}
            tabBarActiveTextColor={"#f00"}
            tabBarInactiveTextColor={"rgba(255,0,0,0.3)"}
            tabBarTextStyle={{
              fontSize: 16,
              fontWeight: "500"
            }}
            style={{ backgroundColor: "#fff" }}
            contentProps={{ backgroundColor: "#ffff", marginTop: 0 }}
            renderTabBar={() =>
              <ScrollableTabBar
                underlineHeight={3}
                style={{ borderBottomColor: "#eee" }}
                tabsContainerStyle={{
                  paddingLeft: 30,
                  paddingRight: 30,
                  backgroundColor: "#fff"
                }}
                tabStyle={{
                  paddingBottom: 0,
                  borderBottomWidth: 0,
                  paddingTop: 0,
                  paddingLeft: 50,
                  paddingRight: 50
                }}
              />}
          >
            <SignIn tabLabel="Login" />
            <SignUp tabLabel="Sign Up" />
          </ScrollableTabView>
        </View>
      </View>
    );
  }
}

export default Auth;
