import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Image,
  StatusBarIOS,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  DeviceEventEmitter,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Login } from "@data/actions";

import ButtonRound from "@commons/components/Buttons/ButtonRound";
import { Actions } from "react-native-router-flux";
import IconInput from "@commons/components/IconInput";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{ marginTop: -15 }}>
            <IconInput
              onChangeText={email => this.setState({ email })}
              placeholder="Email"
              image={require("@images/icon-user.png")}
            />

            <IconInput
              onChangeText={password => this.setState({ password })}
              placeholder="Password"
              image={require("@images/icon-password.png")}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              alignSelf: "flex-end",
              marginTop: 10,
              marginBottom: 10
            }}
          >
            <Text style={{ color: "#aaa", marginRight: 5, fontSize: 12 }}>
              Forgot Password?
            </Text>
          </View>

          <ButtonRound
            text="Enter"
            onPress={() => this.props.signInUser(this.state)}
          />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToprops = state => {
  return state.AuthState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ signInUser: Login }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(SignIn);

const styles = {
  container: {
    flex: 1,
    marginTop: 40,
    paddingTop: 30,
    paddingRight: 30,
    paddingBottom: 30,
    paddingLeft: 30,
    backgroundColor: "white"
  }
};
