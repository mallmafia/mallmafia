import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Image,
  StatusBarIOS,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  DeviceEventEmitter
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Register } from "@data/actions";

import ButtonRound from "@commons/components/Buttons/ButtonRound";
import IconInput from "@commons/components/IconInput";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      phone: "",
      email: "",
      password: ""
    };
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={{ marginTop: -20 }}>
            <IconInput
              onChangeText={name => this.setState({ name })}
              placeholder="Name"
              image={require("@images/icon-phone.png")}
            />
            <IconInput
              onChangeText={phone => this.setState({ phone })}
              placeholder="phone"
              image={require("@images/icon-phone.png")}
            />
            <IconInput
              onChangeText={password => this.setState({ password })}
              placeholder="Password"
              image={require("@images/icon-password.png")}
            />
            <IconInput
              onChangeText={email => this.setState({ email })}
              placeholder="Email"
              image={require("@images/icon-password.png")}
            />
          </View>
          <ButtonRound
            onPress={() => this.props.registerUser(this.state)}
            text="Registration"
          />

          <Text
            style={{
              color: "white",
              opacity: 0.7,
              marginRight: 5,
              fontSize: 15
            }}
          >
            already have an account
          </Text>
          <TouchableOpacity
            onPress={Actions.login}
            style={{ alignSelf: "flex-end", marginRight: 15 }}
          >
            <Text style={styles.registerLink}>Sign in now</Text>
          </TouchableOpacity>

          <Text style={{ color: "#aaa", textAlign: "center", padding: 12 }}>
            By clicking "Registration" I agree to MallMafia{" "}
            <Text style={{ color: "#3071D0" }}>Terms of Service</Text>
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToprops = state => {
  return state.AuthState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ registerUser: Register }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(SignUp);

const styles = {
  container: {
    flex: 1,
    marginTop: 40,
    paddingTop: 30,
    paddingRight: 30,
    paddingBottom: 30,
    paddingLeft: 30,
    backgroundColor: "white"
  },
  registerLink: {
    color: "rgba(255,255,255,0.9)",
    alignSelf: "center",
    fontSize: 15,
    textDecorationLine: "underline"
  }
};
