import React, { Component } from "react";
import { Text, View, Modal, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { OFF_ALERT } from "@data/constants";

class Alert extends Component {
  renderButton = d => {
    return d.map((data, i) => {
      const button_border =
        i === d.length - 1
          ? null
          : { borderRightWidth: 1, borderColor: "rgba(0,0,0,0.1)" };
      return (
        <TouchableOpacity
          key={i}
          style={[styles.buttonStyle, button_border]}
          onPress={data.onPress}
        >
          <Text style={styles.button_textStyle}>
            {data.label.toUpperCase()}
          </Text>
        </TouchableOpacity>
      );
    });
  };

  render() {
    const { visible, text, buttons } = this.props;
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={visible}
        onRequestClose={() => this.props.onBack()}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <View style={styles.text_container}>
              <View style={styles.header}>
                <Text style={styles.header_text}>Alert</Text>
              </View>
              <Text style={styles.text}>
                {text}
              </Text>
            </View>
            <View style={styles.button_container}>
              {this.renderButton(buttons)}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToprops = state => {
  return state.EventState.alert;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { onBack: () => dispatch({ type: OFF_ALERT }) },
    dispatch
  );
};

export default connect(mapStateToprops, mapDispatchToProps)(Alert);

const styles = {
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "relative",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    paddingBottom: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  header_text: {
    fontSize: 16,
    color: "#f44242"
  },
  content: {
    width: "80%",
    borderRadius: 5,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    color: "#000"
  },
  text_container: {
    padding: 10
  },
  button_container: {
    flexDirection: "row",
    borderTopWidth: 1,
    borderColor: "rgba(0,0,0,0.1)"
  },
  buttonStyle: {
    flex: 1,
    padding: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  button_textStyle: {
    fontSize: 16,
    color: "#f44242"
  }
};
