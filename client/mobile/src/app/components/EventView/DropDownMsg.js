import React, { Component } from "react";
import { View, Text, Animated, Easing, Dimensions } from "react-native";

const { w, h } = Dimensions.get("window");

class DropDownMsg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      animatedValue: new Animated.Value(0)
    };
  }

  componentDidMount() {
    Animated.timing(this.state.animatedValue, {
      toValue: 1,
      easing: Easing.bounce,
      duration: 1500
    }).start();
  }

  onLayoutChange = event => {
    const { layout: { height } } = event.nativeEvent;
    this.setState({ height: height });
  };

  render() {
    const { height } = this.state;
    const bottom = this.state.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -height]
    });
    return (
      <Animated.View
        style={[styles.container, { bottom: bottom }]}
        onLayout={this.onLayoutChange}
      >
        <Text style={styles.text}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
      </Animated.View>
    );
  }
}

export default DropDownMsg;

const styles = {
  container: {
    position: "absolute",
    width: "100%",
    backgroundColor: "rgba(0,0,0,0.8)"
  },
  content: {
    backgroundColor: "#fff"
  },
  text: {
    color: "#fff",
    textAlign: "center"
  }
};
