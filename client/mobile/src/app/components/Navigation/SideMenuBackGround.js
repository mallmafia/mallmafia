import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";
import css from "./style";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/Ionicons";
import { AsyncSet, AsyncGet } from "@utils";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Fetch, Flush } from "@data/actions";
class SideMenuIcons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: ""
    };
  }
  componentDidMount() {
    if (!this.props.entities.user) {
      this.props.Fetch(
        { method: "get", route: `user`, name: "user" },
        () => null
      );
    }
  }
  render() {
    const userDetails = this.props.entities.user;
    console.log(this.props.entities.user);
    if (this.props.entities.user) {
      return (
        <ScrollView>
          <View style={[css.sideMenuLeft, this.props.menuBody]}>
            <Image
              source={require("@images/menubackground.png")}
              style={css.menuBg}
            />

            <View style={css.profileCenter}>
              <Image
                style={css.avatarLeft}
                source={{
                  uri:
                    "https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png"
                }}
              />
              <Text style={[css.fullname, this.props.textColor]}>
                Hello{" "}
                {userDetails.fullName ? (
                  this.props.entities.user.fullName
                ) : (
                  "User"
                )}
              </Text>
              <Text style={[css.email, this.props.textColor]}>
                {userDetails.email ? userDetails.email : ""}
              </Text>
              {userDetails.phone ? (
                <Text style={[css.address, this.props.textColor]}>
                  {userDetails.phone}
                  <Icon name={"ios-pin-outline"} style={[css.iconSmall]} />
                </Text>
              ) : null}
            </View>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={() => Actions.cart({ cart: userDetails.cart })}
            >
              <Icon
                name={"ios-cart-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>Cart</Text>

              <View style={css.badge}>
                <Image
                  source={require("@images/circle.png")}
                  style={css.badgeIcon}
                >
                  <Text style={css.badgeText}>
                    {this.props.entities.cart ? (
                      this.props.entities.cart.length
                    ) : userDetails.cart ? (
                      userDetails.cart.length
                    ) : (
                      0
                    )}
                  </Text>
                </Image>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.market}
            >
              <Icon
                name={"ios-cart-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                Market
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.orders}
            >
              <Icon
                name={"ios-cart-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                Orders
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.auth}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>Auth</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.profile}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                Profile
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.map}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                NavMap
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[css.menuRowLeft, css.menuSignOut, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={() => {
                AsyncStorage.removeItem("@jwtkey", () => null);
                Actions.market();
                this.props.Flush({ name: "user" });
              }}
            >
              <Icon
                name={"ios-log-out-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text
                style={[
                  css.menuLinkLeft,
                  css.logoutLinkLeft,
                  this.props.textColor
                ]}
              >
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView>
          <View style={[css.sideMenuLeft, this.props.menuBody]}>
            <Image
              source={require("@images/menubackground.png")}
              style={css.menuBg}
            />
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.market}
            >
              <Icon
                name={"ios-cart-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                Market
              </Text>

              {/* <View style={css.badge}>
                  <Image
                    source={require("@images/circle.png")}
                    style={css.badgeIcon}
                  >
                    <Text style={css.badgeText}>{userDetails}</Text>
                  </Image>
                </View> */}
            </TouchableOpacity>

            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.auth}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>Auth</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.profile}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                Profile
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[css.menuRowLeft, this.props.rowStyle]}
              underlayColor="#2D2D30"
              onPress={Actions.map}
            >
              <Icon
                name={"ios-settings-outline"}
                style={[css.icon, this.props.iconStyle]}
              />
              <Text style={[css.menuLinkLeft, this.props.textColor]}>
                NavMap
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    }
  }
}
const mapStateToprops = state => {
  // if (state.FetchState.hasOwnProperty("user")) return state.FetchState.user;
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Fetch: Fetch, Flush: Flush }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(SideMenuIcons);
