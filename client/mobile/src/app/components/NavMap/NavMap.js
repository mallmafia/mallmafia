import React, { Component } from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";
const { width, height, scale } = Dimensions.get("window");
const RouterComponent = props => {
  return (
    <TouchableOpacity onPress={props.actions}>
      <View
        style={{
          width: width / 3,
          alignItems: "center",
          justifyContent: "center",
          height: height / 8,
          borderWidth: 2,
          borderColor: "#fff"
        }}
      >
        <Text>
          {" "}{props.text}{" "}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
export default class NavMap extends Component {
  render() {
    return (
      <View
        style={{
          flexWrap: "wrap",
          flexDirection: "row",
          alignItems: "stretch",
          backgroundColor: "#eee"
        }}
      >
        <RouterComponent text="Home" actions={Actions.home} />
        <RouterComponent text="Market" actions={Actions.market} />
        <RouterComponent text="Category" actions={Actions.product} />
        <RouterComponent text="Product" actions={Actions.product} />
        <RouterComponent text="Login/Register" actions={Actions.auth} />
        <RouterComponent text="Cart" actions={Actions.product} />
        <RouterComponent text="Stores" actions={Actions.stores} />
        <RouterComponent text="Products List" actions={Actions.products} />
        <RouterComponent text="Preorder" actions={Actions.product} />
        <RouterComponent text="Profile" actions={Actions.profile} />
        <RouterComponent text="Compare" actions={Actions.product} />
        <RouterComponent text="Order" actions={Actions.product} />
        <RouterComponent text="Tracking" actions={Actions.product} />
        <RouterComponent text="Vouchers" actions={Actions.product} />
      </View>
    );
  }
}
