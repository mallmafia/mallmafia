import React, { Component } from "react";
import { Navigator, Text, View } from "react-native";
import { Scene, Schema, Actions, Animations } from "react-native-router-flux";
import MenuSide from "./Navigation/MenuWide";
import NavMap from "./NavMap/NavMap";
import Product from "./Product/Product";
import Home from "./Home";
import Profile from "./Profile/UserScreen";
import Orders from "./Profile/Orders";
import Rewards from "./Profile/Rewards";
import Settings from "./Profile/Settings";
import MarketScreen from "./Market";
import Auth from "./Auth/Auth";
import Shops from "./Shops/Shops";
import CategoryShops from "./Shops/CategoryShops";
import Shop from "./Shops/Shop";
import Cart from "./Cart/Cart";
import SearchProduct from "./Search/SearchProduct";
import SearchMap from "./Search/SearchMap";
import SearchInStore from "./Search/SearchInStore";
import ProductsList from "./Products/ProductsList";
import AppEventEmitter from "@utils/AppEventEmitter";
import DeliverHome from "./Cart/DeliverHome";
export default class RootRouter extends Component {
  constructor(props) {
    super(props);
    this.state = { menuStyle: 0 };
  }

  changeMenuStyle(data) {
    this.setState({ menuStyle: data.menuId });
  }

  componentDidMount() {
    AppEventEmitter.addListener(
      "app.changeMenuStyle",
      this.changeMenuStyle.bind(this)
    );
  }

  render() {
    const scenes = Actions.create(
      <Scene key="scene">
        <Scene key="market" component={MarketScreen} initial />
        <Scene key="product" component={Product} title="Product" />
        <Scene key="home" component={Home} />
        <Scene key="map" component={NavMap} />
        <Scene key="profile" component={Profile} />
        <Scene key="orders" component={Orders} />
        <Scene key="rewards" component={Rewards} />
        <Scene key="searchmap" component={SearchMap} />
        <Scene key="settings" component={Settings} />
        <Scene key="auth" component={Auth} />
        <Scene key="stores" component={Shops} />
        <Scene key="categoryshops" component={CategoryShops} />
        <Scene key="store" component={Shop} />
        <Scene key="search" component={SearchProduct} />
        <Scene key="cart" component={Cart} />
        <Scene key="products" component={ProductsList} />
        <Scene key="searchinstore" component={SearchInStore} />
        <Scene key="deliverhome" component={DeliverHome} />
      </Scene>
    );

    return <MenuSide ref="menuDefault" scenes={scenes} />;
  }
}
