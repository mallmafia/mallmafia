import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  Button,
  DeviceEventEmitter,
  StyleSheet
} from "react-native";
import Header from "@commons/components/Header";
import css from "./style";
import Orders from "./Orders";
import Settings from "./Settings";
import Rewards from "./Rewards";
import order from "./myorder";
import { AsyncGet } from "@utils/AsyncGet";
import { Actions } from "react-native-router-flux";
import { Fetch } from "@data/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const { width, height } = Dimensions.get("window");
class Profile extends Component {
  componentDidMount() {
    this.props.Fetch(
      { method: "get", route: "user", name: "profile" },
      () => null
    );
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="My Account" searchButton cartButton />
        <ScrollView>
          <View style={styles.bottomBump}>
            <Text
              style={{
                color: "#ffff",
                fontSize: 20,

                marginLeft: 30,
                width: width / 2.2
              }}
            >
              {this.props.entities ? this.props.entities.profile ? (
                this.props.entities.profile.fullName
              ) : null : null}
            </Text>
            <Text
              style={{
                color: "#ffff",
                fontSize: 20,
                marginLeft: 30
              }}
            >
              {this.props.entities ? this.props.entities.profile ? (
                this.props.entities.profile.email
              ) : null : null}
            </Text>
            <Text
              style={{
                color: "#ffff",
                fontSize: 20,
                marginLeft: 30,
                width: width / 2.2
              }}
            >
              {this.props.entities ? this.props.entities.profile ? (
                this.props.entities.profile.phone
              ) : null : null}
            </Text>
          </View>
          <View style={order.card}>
            <View style={order.row}>
              <View style={order.content}>
                <Text style={order.name} style={{ color: "#000" }}>
                  My Orders
                </Text>
              </View>
              <TouchableOpacity onPress={Actions.orders}>
                <View>
                  <Text style={{ color: "#000" }}>Details</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={order.line} />
          <View style={order.lineFinishHalf} />
          <View style={order.card}>
            <View style={order.row}>
              <View style={order.content}>
                <Text style={order.name} style={{ color: "#000" }}>
                  Rewards
                </Text>
              </View>
              <TouchableOpacity onPress={Actions.rewards}>
                <View>
                  <Text style={{ color: "#000" }}>Details</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={order.line} />
          <View style={order.lineFinishHalf} />
          <View style={order.card}>
            <View style={order.row}>
              <View style={order.content}>
                <Text style={order.name} style={{ color: "#000" }}>
                  Settings
                </Text>
              </View>
              <TouchableOpacity onPress={Actions.settings}>
                <View>
                  <Text style={{ color: "#000" }}>Details</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={order.line} />
          <View style={order.lineFinishHalf} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomBump: {
    backgroundColor: "#f44242",
    width,
    height: height / 4
  },
  dp: {
    height: 160,
    width: 160,
    borderRadius: 100,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10
  }
});

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Fetch: Fetch }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(Profile);
