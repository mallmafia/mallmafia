import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Image,
  StatusBarIOS,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  DeviceEventEmitter
} from "react-native";
import login from "@styles/login";
import ButtonRoundRed from "@commons/components/Buttons/ButtonRound";
import { Actions } from "react-native-router-flux";
import IconInput from "@commons/components/IconInput";

export default class SignIn extends Component {
  render() {
    return (
      <ScrollView>
        <View style={login.container}>
          <View style={{ marginTop: -15 }}>
            <IconInput
              placeholder="Email"
              image={require("@images/icon-user.png")}
            />

            <IconInput
              placeholder="Password"
              image={require("@images/icon-password.png")}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              alignSelf: "flex-end",
              marginTop: 10,
              marginBottom: 10
            }}
          >
            <Text style={{ color: "#aaa", marginRight: 5, fontSize: 12 }}>
              Forgot Password?
            </Text>
          </View>

          <ButtonRoundRed
            text="Enter"
            onPress={() => Actions.home({ data: this.state.value })}
          />
        </View>
      </ScrollView>
    );
  }
}
