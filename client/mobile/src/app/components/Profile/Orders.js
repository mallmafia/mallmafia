import React, { Component } from "react";
import {
  ListView,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Dimensions
} from "react-native";
import Header from "@commons/components/Header";
import order from "./myorder";
import { Actions } from "react-native-router-flux";
import button from "./button";
import { Fetch } from "@data/actions";
import { connect } from "react-redux";
const { width, height } = Dimensions.get("window");
import moment from "moment";

class MyOrder extends Component {
  componentWillMount() {}
  componentDidMount() {
    this.props.fetchOrders();
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="My Orders" searchButton cartButton />

        <ScrollView>
          <View style={{ backgroundColor: "#F2F2F2" }}>
            {this.props.entities.orders ? (
              this.props.entities.orders.map(order =>
                order.order_items.map(item => (
                  <View style={orderstyle.card}>
                    <View style={[order.content, { flexDirection: "row" }]}>
                      <Image
                        style={orderstyle.image}
                        source={{ uri: item.product.showcase }}
                      />
                      <View style={{ marginLeft: 5 }}>
                        <Text style={orderstyle.name}>
                          {item.product.name.slice(0, 13)}
                        </Text>
                        <Text style={orderstyle.id}>Order id: {order._id}</Text>
                        <Text style={orderstyle.id}>
                          Order type: {order.order_type}
                        </Text>
                        <Text style={orderstyle.id}>
                          Sold By: {item.product.store_name}
                        </Text>
                        <Text style={orderstyle.date}>
                          Orderd on:{" "}
                          {moment(order.date).format("MMM Do YY, hh:mm a")}
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between"
                          }}
                        >
                          <Text style={orderstyle.quantity}>
                            Quantity: {item.quantity}
                          </Text>
                          <View style={order.buttons}>
                            <TouchableOpacity>
                              <Text style={button.small}>DETAIL</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>

                    <View
                      style={{
                        alignItems: "flex-start",
                        flexDirection: "row",
                        margin: 6
                      }}
                    >
                      <Text style={orderstyle.quantity}>
                        &#8377;{item.quantity * item.product.price}
                      </Text>
                      <Text style={{ fontSize: 14 }}>Orderd status: </Text>
                      <Text
                        style={{
                          backgroundColor: "#e1e1e1",
                          borderRadius: 10,
                          padding: 4
                        }}
                      >
                        {item.order_status}
                      </Text>
                      {item.order_message ? (
                        <Text>Order Message: {item.order_message}</Text>
                      ) : null}
                    </View>
                  </View>
                ))
              )
            ) : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const orderstyle = {
  card: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 18,
    paddingTop: 16,
    paddingRight: 12,
    paddingBottom: 4,
    paddingLeft: 12,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 0,
    borderRadius: 9
  },
  image: {
    resizeMode: "contain",
    width: width / 4 + 30,
    height: 100,
    borderRadius: 3,
    overflow: "hidden"
  },
  name: {
    fontSize: 13,
    color: "#333",
    fontWeight: "600",
    paddingBottom: 5
  },
  id: {
    fontSize: 11,
    color: "#333",
    paddingBottom: 5
  },
  date: {
    fontSize: 10,
    color: "#333",
    paddingBottom: 5
  },
  quantity: {
    fontSize: 15,
    color: "#333",
    paddingBottom: 5,
    marginRight: 15
  },
  line: {
    borderTopWidth: 2,
    borderColor: "#f44242",
    width: width - 20,
    position: "relative",
    alignSelf: "center",
    marginTop: 15
  },
  fullline: {
    borderTopWidth: 2,
    borderColor: "#f44242",
    width: width - 20,
    position: "absolute",
    left: 0,
    marginTop: -2
  },
  icon: {
    flexDirection: "row",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "space-between",
    width: width - 60,
    top: -11,
    paddingTop: 6
  },
  horizontal: {
    flexDirection: "column",
    alignItems: "center",
    paddingTop: 3
  },
  round: {
    height: 16,
    width: 16,
    backgroundColor: "#f44242",
    borderRadius: 8,
    marginTop: -8
  },
  greyround: {
    height: 16,
    width: 16,
    backgroundColor: "#ddd",
    borderRadius: 8,
    marginTop: -8
  },
  halfgrey: {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: "#f44242",
    borderWidth: 6,
    borderColor: "#ddd",
    marginLeft: -2
  },
  ordertext: {
    fontSize: 10,
    fontWeight: "500",
    color: "#666",
    paddingTop: 8
  }
};
const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchOrders: () =>
      dispatch(
        Fetch(
          { method: "GET", route: "user/order", name: "orders" },
          () => null
        )
      )
  };
};
export default connect(mapStateToprops, mapDispatchToProps)(MyOrder);
