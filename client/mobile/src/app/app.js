import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Provider } from "react-redux";
import RootRouter from "./components/RootRouter";
import store from "@data/store";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootRouter/>
      </Provider>
    );
  }
}
