"use strict";

import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import post from "./style";
import LinearGradient from "react-native-linear-gradient";

import TimeAgo from "react-native-timeago";
import Constants from "@constants/Constants";
import { Actions } from "react-native-router-flux";
import Tools from "@utils/tools";
import CommentIcons from "@commentIcons/Index";

export default class Post extends Component {
  viewPost() {
    Actions.postDetails({ post: this.props.post });
  }

  viewCategoryDetail(id) {
    Tools.viewCateDetail(id);
  }

  render() {
    const data = this.props.post;
    const categories = this.props.categories;
    const imageURL = Tools.getImage(data);
    const authorName = data._embedded.author[0]["name"];
    const postTitle =
      typeof data.title.rendered == "undefined" ? "" : data.title.rendered;
    const commentCount =
      typeof data._embedded.replies == "undefined"
        ? 0
        : data._embedded.replies[0].length;

    if (this.props.layout == Constants.Post.layout_oneColumn) {
      return (
        <TouchableOpacity
          elevation={10}
          style={post.cardNews}
          onPress={this.viewPost.bind(this)}
        >
          <View style={post.cardView}>
            <Image style={post.largeImage} source={{ uri: imageURL }} />

            <LinearGradient
              style={post.linearGradient}
              colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.7)"]}
            >
              <Text style={post.newsTitle}>
                {Tools.getDescription(postTitle)}
              </Text>
            </LinearGradient>

            <View style={post.description}>
              <Text style={post.author}>
                <TimeAgo time={data.date} hideAgo={true} /> by @{authorName}
              </Text>
              <CommentIcons post={post} comment={commentCount} />
            </View>
          </View>
        </TouchableOpacity>
      );
    } else if (this.props.layout == Constants.Post.layout_twoColumn) {
      return (
        <TouchableOpacity
          style={post.smCardNews}
          onPress={this.viewPost.bind(this)}
        >
          <View style={post.cardView}>
            <Image style={post.smImage} source={{ uri: imageURL }} />
            <View style={post.smDescription}>
              <Text style={post.smTitle}>
                {Tools.getDescription(postTitle)}
              </Text>
              <Text style={post.smAuthor}>
                <TimeAgo time={data.date} hideAgo={true} /> by @{authorName}
              </Text>
            </View>
            <CommentIcons
              hideOpenIcon={true}
              post={post}
              style={post.smShareIcons}
              comment={commentCount}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (this.props.layout == Constants.Post.layout_simpleListView) {
      return (
        <TouchableOpacity
          style={post.panelList}
          onPress={this.viewPost.bind(this)}
        >
          <Image source={{ uri: imageURL }} style={post.imageList} />
          <View style={post.titleList}>
            <Text style={post.nameList}>
              {Tools.getDescription(postTitle, 300)}
            </Text>

            <View style={{ flexDirection: "row", marginLeft: 5, marginTop: 8 }}>
              <Text style={{ fontSize: 12, color: "#999" }}>
                <TimeAgo
                  style={post.timeList}
                  time={data.date}
                  hideAgo={true}
                />{" "}
                by @{authorName}
              </Text>
              <TouchableOpacity
                onPress={this.viewCategoryDetail.bind(this, data.categories[0])}
              >
                <Text style={post.category}>
                  {categories[data.categories[0]]}
                </Text>
              </TouchableOpacity>
            </View>
            <CommentIcons
              hideOpenIcon={true}
              post={post}
              comment={commentCount}
            />
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity style={post.panel} onPress={this.viewPost.bind(this)}>
        <Image source={{ uri: imageURL }} style={post.imagePanel} />
        <Text style={post.name}>
          {Tools.getDescription(postTitle)}
        </Text>
        <Text style={post.time}>
          <TimeAgo time={data.date} hideAgo={true} />
        </Text>
      </TouchableOpacity>
    );
  }
}
