import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  Dimensions
} from "react-native";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import AppEventEmitter from "@utils/AppEventEmitter";
import IconImage from "./IconImage";
import Constants from "@constants/Constants";
import Color from "@constants/Color";
import Modal from "react-native-modal";
import ModalPicker from "react-native-modal-picker";
import { Fetch } from "@data/actions";
import store from "@data/store";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AsyncSet, AsyncGet } from "@utils";
const { width, height, scale } = Dimensions.get("window");
(vw = width / 100),
  (vh = height / 100),
  (vmin = Math.min(vw, vh)),
  (vmax = Math.max(vw, vh));

const css = {
  icon: {
    fontSize: 30,
    color: "#fff"
  },
  iconBack: {
    marginLeft: 0,
    position: "absolute",
    left: -12,
    top: -7
  },
  icon_white: {
    tintColor: "#fff"
  },
  imageIcon: {
    width: 16,
    height: 16
  },
  toolbarMenu: {
    height: 110,
    backgroundColor: "#f44242",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
    paddingTop: 13,
    paddingRight: 15
  },
  toolbarTitleView: {
    position: "absolute",
    top: 27,
    left: 0,
    width: width - 160,
    marginLeft: 80,
    marginRight: 80,
    alignItems: "center"
  },
  toolbarTitle: {
    color: "#fff",
    fontSize: 16,
    alignSelf: "center"
  }
};
let data = [];
class Header extends Component {
  constructor() {
    super();
    this.state = {
      textInputValue: ""
    };
  }
  componentDidMount() {
    // this.props.Fetch(
    //   { method: "get", route: "user", name: "cart" },
    //   data => null
    // );

    if (!this.props.static) {
      ////////////  FETCHING ALL MALLS /////////////////////////////////
      this.props.Fetch({ method: "get", route: "mall", name: "malls" }, data =>
        AsyncSet({ key: "@malls", data: data }, () => null)
      );
    }
    /////////////////////  GETTING THE PERSISTED MALL FROM ASYNCSTORAGE //////////
    AsyncGet("@mall", mall => {
      this.setState({ initMall: mall.label });
      return;
    });
  }

  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });
  open() {
    AppEventEmitter.emit("hamburger.click");
  }

  render() {
    const { entities } = this.props;
    let index = 0;

    if (entities.malls) {
      data = entities.malls.map(mall => {
        return { label: mall.name, key: index++ };
      });
    }
    const self = this;
    const homeButton = function() {
      if (self.props.backButton) {
        return (
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TouchableOpacity
              onPress={self.props.action ? self.props.action : Actions.pop}
            >
              <View style={{ marginLeft: 15, height: 18, width: 30 }}>
                <Icon
                  name={"arrow-left"}
                  style={[css.icon, css.iconBack, css.icon_white]}
                />
              </View>
            </TouchableOpacity>
            <Text style={css.toolbarTitle}>{self.props.secondaryname}</Text>
          </View>
        );
      }
      return (
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity onPress={self.open}>
            <Image
              source={require("@images/icon-nav.png")}
              style={[css.imageIcon, css.iconHome, css.icon_white]}
            />
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View>
        <View style={css.toolbarMenu}>
          <StatusBar backgroundColor="#f44242" />
          {homeButton()}

          <View
            style={[
              css.toolbarTitleView,
              { top: this.props.mallSelect ? 30 : 50 }
            ]}
          >
            <Text style={css.toolbarTitle}>{this.props.name}</Text>
          </View>
          {this.props.mallSelect ? (
            <View
              style={[css.toolbarTitleView, { top: 50, flexDirection: "row" }]}
            >
              <IconImage
                image={{
                  uri:
                    "https://cdn4.iconfinder.com/data/icons/pictype-free-vector-icons/16/location-alt-512.png"
                }}
              />
              <ModalPicker
                style={{ padding: 10 }}
                selectTextStyle={css.toolbarTitle}
                cancelTextStyle={{ fontSize: 20 }}
                overlayStyle={{ backgroundColor: "rgba(0,0,0,0.8)" }}
                sectionStyle={{ backgroundColor: "black" }}
                sectionTextStyle={{ fontSize: 90 }}
                cancelStyle={{ backgroundColor: "#d3cfcf", borderRadius: 7 }}
                optionStyle={{
                  backgroundColor: "#d3cfcf",
                  borderRadius: 7,
                  marginTop: 5
                }}
                optionTextStyle={{ color: "black" }}
                data={data}
                initValue={
                  this.state.initMall ? this.state.initMall : "Select the mall"
                }
                onChange={option => {
                  AsyncSet({ key: "@mall", data: option }, () => null);
                  this.props.Fetch(
                    {
                      method: "get",
                      route: `mall/${option.key + 1}`,
                      name: "selectedMall"
                    },
                    data => null
                  );
                }}
              />
            </View>
          ) : null}
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            {/* wordpress menu */}
            {self.props.cardButton && (
              <IconImage
                action={Actions.cardView.bind(this)}
                image={{ uri: Constants.icons.card }}
              />
            )}
            {self.props.listButton && (
              <IconImage
                action={Actions.twoColumnView.bind(this)}
                image={{ uri: Constants.icons.list }}
              />
            )}
            {self.props.newsLayoutButton && (
              <IconImage
                action={Actions.oneColumnView.bind(this)}
                image={{ uri: Constants.icons.layout }}
              />
            )}
            {self.props.listViewButton && (
              <IconImage
                action={Actions.simpleListView.bind(this)}
                image={{ uri: Constants.icons.listView }}
              />
            )}

            {/* e-commerce menu */}
            {self.props.gridButton ? (
              <IconImage
                action={Actions.product}
                image={require("@images/icon-list.png")}
              />
            ) : null}
            {self.props.heartButton ? (
              <IconImage
                action={Actions.wishlist}
                image={require("@images/icon-heart.png")}
              />
            ) : null}
            {self.props.cartButton ? (
              <View style={{ position: "absolute" }}>
                <View style={{ right: 25, top: 10 }}>
                  <IconImage
                    action={Actions.cart}
                    image={require("@images/icon-bag.png")}
                  />
                </View>
                <View
                  style={{
                    backgroundColor: "#fff",
                    width: 15,
                    height: 15,
                    alignItems: "center",
                    borderRadius: 10,
                    bottom: 35,
                    right: 26
                  }}
                >
                  <Text
                    style={{
                      color: "#f00",
                      fontWeight: "600",
                      fontSize: 10
                    }}
                  >
                    {store.getState().FetchState.entities
                      .user ? store.getState().FetchState.entities.user.cart ? (
                      store.getState().FetchState.entities.user.cart.length
                    ) : (
                      0
                    ) : (
                      0
                    )}
                  </Text>
                </View>
              </View>
            ) : null}
            {self.props.searchButton ? (
              <TouchableOpacity>
                <IconImage
                  image={require("@images/icon-search.png")}
                  action={Actions.search}
                />
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToprops = state => {
  return state.FetchState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ Fetch }, dispatch);
};

export default connect(mapStateToprops, mapDispatchToProps)(Header);
