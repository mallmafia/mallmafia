'use strict';

import React, { Component } from 'react';
import { Text, View, TextInput, Image } from 'react-native';

const css = {
  buttonRound: {
    position: 'relative',
    borderColor: '#ddd',
    borderWidth: 0.8,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    marginTop: 10,
    marginRight: 8,
    marginBottom: 8,
    marginLeft: 8,
    paddingBottom: 8,
  },
  inputIcon: {
    position: 'absolute',
    left: 0,
    top: 6,
    resizeMode: 'contain',
    height: 24,
    width: 24,
  },
  textInputDark: {
    height: 40,
    backgroundColor: 'transparent',
    color: 'rgba(0,0,0,0.9)',
    paddingLeft: 40,
  },
};

export default class IconInput extends Component {
  render() {
    return (
      <View style={css.buttonRound}>
        <Image source={this.props.image} style={css.inputIcon} />

        <TextInput
          underlineColorAndroid="rgba(0,0,0,0)"
          style={css.textInputDark}
          placeholderTextColor={'#aaa'}
          {...this.props}
        />
      </View>
    );
  }
}
