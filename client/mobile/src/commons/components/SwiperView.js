import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ListView,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import Swiper from 'react-native-swiper';

const { width, height, scale } = Dimensions.get('window');

export default class SwiperView extends Component {
  render() {
    return (
      <View style={{ height: 300 }}>
        <Swiper
          dot={
            <View
              style={{
                backgroundColor: '#F00',
                width: 8,
                height: 8,
                borderRadius: 4,
                marginLeft: 4,
                marginRight: 4,
              }}
            />
          }
          activeDot={
            <View
              style={{
                backgroundColor: '#fff',
                width: 8,
                height: 8,
                borderRadius: 4,
                marginLeft: 4,
                marginRight: 4,
              }}
            />
          }
          // paginationStyle={{ top: -300, left: 300 }}
          autoplay
          autoplayDirection
          autoplayTimeout={3.0}
          // style={{ height: 400 }}
        >
          {this.props.image_array.map(data =>
            <View key={data.key}>
              <Image
                source={data.image}
                style={{
                  width,
                  height: 300,
                  resizeMode: 'contain',
                }}
              />
            </View>
          )}
        </Swiper>
      </View>
    );
  }
}
