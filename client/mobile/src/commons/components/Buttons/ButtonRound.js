import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const buttonRound = {
  backgroundColor: '#f44242',
  paddingTop: 12,
  paddingRight: 12,
  paddingBottom: 12,
  paddingLeft: 12,
  borderColor: '#ddd',
  borderWidth: 1,
  alignSelf: 'center',
  borderRadius: 20,
  marginTop: 12,
  marginLeft: 10,
  marginRight: 10,
  width: width - 30,
};
const buttonRoundText = {
  color: '#fff',
  alignSelf: 'center',
  fontSize: 14,
};
module.exports = React.createClass({
  render: function() {
    return (
      <TouchableOpacity
        style={[buttonRound, this.props.style]}
        onPress={this.props.onPress}
      >
        <Text style={buttonRoundText}>
          {this.props.text}
        </Text>
      </TouchableOpacity>
    );
  },
});
