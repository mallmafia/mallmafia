import React, { Component } from "react";
import { View } from "react-native";
import css from "./style";
import Color from "@constants/Color";
import Spinner from "react-native-spinkit";

export default class Index extends Component {
  render() {
    return (
      <View
        style={[
          css.spinner,
          typeof this.props.css != "undefined" ? this.props.css : null
        ]}
      >
        <Spinner
          isVisible={this.props.isLoading}
          size={40}
          style={{ marginTop: 30 }}
          type="Circle"
          color={Color.spin}
        />
      </View>
    );
  }
}
