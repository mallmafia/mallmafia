import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, Image } from 'react-native';

export default class IconImage extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.action}
        style={{
          marginLeft: 2,
          marginRight: 0,
          marginTop: 6,
          marginBottom: 10,
          shadowColor: '#000',
          paddingTop: 10,
          paddingRight: 10,
          paddingBottom: 10,
          paddingLeft: 10,
        }}
      >
        <Image
          source={this.props.image}
          style={{
            width: 16,
            height: 16,
            tintColor: '#fff',
          }}
          {...this.props}
        />
      </TouchableOpacity>
    );
  }
}
