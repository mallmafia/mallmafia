import { AsyncStorage } from "react-native";

export const AsyncSet = async (params, callback) => {
  try {
    await AsyncStorage.setItem(params.key, JSON.stringify(params.data), e =>
      console.log(e)
    ).then(() => callback());
  } catch (e) {
    return console.log(e);
  }
};
