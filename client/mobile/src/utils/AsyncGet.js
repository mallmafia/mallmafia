import { AsyncStorage } from "react-native";

export const AsyncGet = async (key, callback) => {
  try {
    var err;
    const data = await AsyncStorage.getItem(key, e => (err = e));
    if (data !== null) {
      callback(JSON.parse(data));
    } else {
      console.log(err);
    }
  } catch (e) {
    return console.log(e);
  }
};
