export default [
  {
    id: 30,
    count: 5,
    description:
      'Browse our collection of family favourite meals for deliciously simple and crowd-pleasing recipes to get supper on the table in a jiffy. Find easy cheesecake recipes to make a show-stopping end to any dinner.',
    link: 'http://inspireui.com/category/deserts/',
    name: 'Deserts',
    slug: 'deserts',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/30' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=30' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 37,
    count: 5,
    description: '',
    link: 'http://inspireui.com/category/kids-menu/',
    name: 'Kids Menu',
    slug: 'kids-menu',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/37' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=37' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 31,
    count: 7,
    description: '',
    link: 'http://inspireui.com/category/pasta/',
    name: 'Pasta',
    slug: 'pasta',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/31' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=31' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 36,
    count: 7,
    description:
      'From festivals in Florida to touring Dracula’s digs in Romania, we round up the best destinations to visit this October. As summer abandons Europe again this October, eke out the last of the rays and raves in Ibiza, where nightclubs will be going out with a bang for the winter break. When the party finally stops head to the island’s north.',
    link: 'http://inspireui.com/category/quick-recipes/',
    name: 'Quick Recipes',
    slug: 'quick-recipes',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/36' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=36' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 32,
    count: 9,
    description:
      'Browse our collection of family favourite meals for deliciously simple and crowd-pleasing recipes to get supper on the table in a jiffy. Find easy cheesecake recipes to make a show-stopping end to any dinner.',
    link: 'http://inspireui.com/category/salads/',
    name: 'Salads',
    slug: 'salads',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/32' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=32' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 33,
    count: 14,
    description:
      'Browse our collection of family favourite meals for deliciously simple and crowd-pleasing recipes to get supper on the table in a jiffy. Find easy cheesecake recipes to make a show-stopping end to any dinner.',
    link: 'http://inspireui.com/category/smoothies/',
    name: 'Smoothies',
    slug: 'smoothies',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/33' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=33' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 1,
    count: 0,
    description: '',
    link: 'http://inspireui.com/category/uncategorized/',
    name: 'Uncategorized',
    slug: 'uncategorized',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/1' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=1' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
  {
    id: 34,
    count: 10,
    description:
      'Browse our collection of family favourite meals for deliciously simple and crowd-pleasing recipes to get supper on the table in a jiffy. Find easy cheesecake recipes to make a show-stopping end to any dinner.',
    link: 'http://inspireui.com/category/video/',
    name: 'Video',
    slug: 'video',
    taxonomy: 'category',
    parent: 0,
    meta: [],
    _links: {
      self: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories/34' }],
      collection: [{ href: 'http://inspireui.com/wp-json/wp/v2/categories' }],
      about: [
        { href: 'http://inspireui.com/wp-json/wp/v2/taxonomies/category' },
      ],
      'wp:post_type': [
        { href: 'http://inspireui.com/wp-json/wp/v2/posts?categories=34' },
      ],
      curies: [
        { name: 'wp', href: 'https://api.w.org/{rel}', templated: true },
      ],
    },
  },
];
