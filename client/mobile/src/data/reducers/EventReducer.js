import {
  ON_LOAD,
  OFF_LOAD,
  ON_DROP,
  OFF_DROP,
  ON_ALERT,
  OFF_ALERT
} from "../constants";

const InitialState = {
  spinner: false,
  drop_error: false,
  alert: { visible: false, text: "", buttons: [] },
  msg: ""
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case ON_LOAD:
      return { ...state, spinner: true };
    case OFF_LOAD:
      return { ...state, spinner: false };
    case ON_DROP:
      return { ...state, drop_error: true, msg: action.payload };
    case OFF_DROP:
      return { ...state, drop_error: false, msg: "" };
    case ON_ALERT:
      return { ...state, alert: { visible: true, ...action.payload } };
    case OFF_ALERT:
      return { ...state, alert: { visible: false, text: "", buttons: [] } };
    default:
      return state;
  }
};
