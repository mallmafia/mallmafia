import {
  NULL,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  MISSING_CREDENTIALS
} from "../constants";

const InitialState = {};

export default (state = InitialState, action) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return { ...state, user: action.payload };
    case REGISTER_FAIL:
      return { ...state, err_msg: action.payload };
    case LOGIN_SUCCESS:
      return { ...state, jwtkey: action.payload.toString() };
    case LOGIN_FAIL:
      return { ...state, err_msg: action.payload };
    case MISSING_CREDENTIALS:
      return { ...state, err_msg: action.payload };
    case NULL:
      return state;
    default:
      return state;
  }
};
