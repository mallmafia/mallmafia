import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import Thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import {
  AuthReducer,
  FetchReducer,
  EventReducer,
  RequestReducer
} from "./reducers";

const reducers = combineReducers({
  AuthState: AuthReducer,
  FetchState: FetchReducer,
  EventState: EventReducer,
  Entities: RequestReducer
});

const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000 });
const store = createStore(
  reducers,
  {},
  composeEnhancers(applyMiddleware(Thunk))
);

export default store;
