import {
  FETCH_SUCCESS,
  FETCH_FAIL,
  API_URL,
  NULL,
  ON_LOAD,
  OFF_LOAD,
  ON_ALERT,
  OFF_ALERT
} from "../constants";
import store from "../store";
import { AsyncStorage } from "react-native";
var type = NULL;

export const Request = (params, callback) => {
  return dispatch => {
    dispatch({ type: ON_LOAD });
    AsyncStorage.getItem("@jwtkey", (err, key) => {
      var method = setMethod(params.method);
      var route = params.route;
      var token = key;
      var body = params.params ? params.params : null;
      var options = {
        method: method,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `JWT ${JSON.parse(key)}`
        }
      };
      if (body) {
        options["body"] = JSON.stringify(body);
      }
      fetch(`${API_URL}/${route}`, options)
        .then(res => res.json())
        .then(res => {
          HandleSuccessResponse(dispatch, params.name, res, callback);
        })
        .catch(error => {
          HandleFailResponse(dispatch, error);
        })
        .done(() => dispatch({ type: OFF_LOAD }));
    });
  };
};

const setMethod = m => {
  if (m == "POST" || m == "post") {
    return "POST";
  } else if (m == "GET" || m == "get") {
    return "GET";
  } else if (m == "PUT" || m == "put") {
    return "PUT";
  } else if (m == "DELETE" || m == "delete") {
    return "DELETE";
  }
  return "GET";
};

const getToken = () => {
  var token = store.getState().AuthState.jwtkey;
  if (token) {
    return token;
  }
  return null;
};

const HandleSuccessResponse = (dispatch, name, data, callback) => {
  if (data) {
    type = FETCH_SUCCESS;
    callback(data);
  }
  return dispatch({ type: type, payload: { name: name, data: data } });
};

const HandleFailResponse = (dispatch, err) => {
  if (err) {
    type = FETCH_FAIL;
    dispatch({
      type: ON_ALERT,
      payload: {
        text:
          "Fetch Failed. Please try again later. If any issues Contact bee@martbee.com.",
        buttons: [{ label: "ok", onPress: () => dispatch({ type: OFF_ALERT }) }]
      }
    });
  }
  return dispatch({ type: type, payload: err });
};
