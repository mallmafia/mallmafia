import { AppRegistry } from "react-native";

import App from "@market_app/app.js";

AppRegistry.registerComponent("MallMafia", () => App);
