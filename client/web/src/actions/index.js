import axios from "axios";

const URL = "http://localhost:3000/s";
export const LOGIN = LOGIN;

export function login(props) {
  const request = axios.post(`${URL}/sign_in`, props);

  return {
    type: LOGIN,
    payload: request,
  };
}
