import React, { Component } from "react";
import { reduxForm } from "redux-form";
import { login } from "../actions/index";

class Login extends Component {
  render() {
    const { fields: { email, password }, handleSubmit } = this.props;
    return (
      <div className="container">
        <div className="container">
          <div className="row">
            <h1 style={{ textAlign: "center", color: "#800e10" }}>Login</h1>
            <div style={{ width: "30%", margin: "25px auto" }}>
              <form onSubmit={handleSubmit(this.props.login)}>
                <div className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    name="email"
                    placeholder="email"
                    {...email}
                  />
                </div>
                <div className="form-group">
                  <input
                    className="form-control"
                    type="password"
                    name="password"
                    placeholder="Password"
                    {...password}
                  />
                </div>
                <div className="form-group">
                  <button
                    className="btn btn-lg btn-primary btn-block"
                    style={{
                      backgroundColor: "#800e10",
                      color: "#ECCF9E",
                      borderColor: "#800e10",
                    }}
                  >
                    Login
                  </button>
                </div>
              </form>
              <a href="/" style={{ color: "#800e10" }}>
                Sign Up
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default reduxForm(
  {
    form: "loginForm",
    fields: ["email", "password"],
  },
  null,
  { login }
)(Login);
