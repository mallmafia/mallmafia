import React, { Component } from "react";

export default class Home extends Component {
  render() {
    return (
      <div>
        <div id="wrapper">
          <div className="navbar navbar-inverse navbar-fixed-top">
            <div className="adjust-nav">
              <div className="navbar-header">
                <button
                  type="button"
                  className="navbar-toggle"
                  data-toggle="collapse"
                  data-target=".sidebar-collapse"
                >
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                <a className="navbar-brand" href="#">
                  <img src="assets/img/logo.png" />
                </a>
              </div>

              <span className="logout-spn">
                <a href="#" style={{ color: "#fff" }}>
                  LOGOUT
                </a>
              </span>
            </div>
          </div>
          <nav className="navbar-default navbar-side" role="navigation">
            <div className="sidebar-collapse">
              <ul className="nav" id="main-menu">
                <li className="active-link">
                  <a href="index.html">
                    <i className="fa fa-desktop " />Dashboard{" "}
                    <span className="badge">Included</span>
                  </a>
                </li>

                <li>
                  <a href="ui.html">
                    <i className="fa fa-table " />Billing{" "}
                    <span className="badge">Included</span>
                  </a>
                </li>
                <li>
                  <a href="blank.html">
                    <i className="fa fa-edit " />Premium users{" "}
                    <span className="badge">Included</span>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-qrcode " />My Link One
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-bar-chart-o" />My Link Two
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-edit " />My Link Three{" "}
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-table " />My Link Four
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-edit " />My Link Five{" "}
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <div id="page-wrapper">
            <div id="page-inner">
              <div className="row">
                <div className="col-lg-12">
                  <h2>ADMIN DASHBOARD</h2>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-lg-12 ">
                  <div className="alert alert-info">
                    <strong>Welcome Jhon Doe ! </strong> You Have No pending
                    Task For Today.
                  </div>
                </div>
              </div>
              <div className="row text-center pad-top">
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="/s/12/myproducts">
                      <i className="fa fa-circle-o-notch fa-5x" />
                      <h4>Check products</h4>
                    </a>
                  </div>
                </div>

                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="/s/12/myproducts">
                      <i className="fa fa-envelope-o fa-5x" />
                      <h4>my products</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="/s/12/addproduct">
                      <i className="fa fa-lightbulb-o fa-5x" />
                      <h4>New Product</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-users fa-5x" />
                      <h4>See Users</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-key fa-5x" />
                      <h4>Store room Products</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-comments-o fa-5x" />
                      <h4>Products in store</h4>
                    </a>
                  </div>
                </div>
              </div>
              <div className="row text-center pad-top">
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-clipboard fa-5x" />
                      <h4>Billing</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-gear fa-5x" />
                      <h4>Settings</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-wechat fa-5x" />
                      <h4>Live Talk</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-bell-o fa-5x" />
                      <h4>Notifications </h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-rocket fa-5x" />
                      <h4>Launch</h4>
                    </a>
                  </div>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                  <div className="div-square">
                    <a href="blank.html">
                      <i className="fa fa-user fa-5x" />
                      <h4>Register User</h4>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
