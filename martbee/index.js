var express = require("express"),
  app = express(),
  port = process.env.PORT || 3001;

// app.use(expressValidator());
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());

app.use(express.static(__dirname + "/shops"));

app.listen(port);
